package de.taliis.plugins.dbc.mixeditor;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import starlight.taliis.core.ZeroTerminatedString;

/**
 * A cute smal renderer for our dbc datas.
 * main feature is of course the background color
 * 
 * @author ganku
 */

public class ColorTableCellRenderer extends DefaultTableCellRenderer {
	/**
	 * Some default colors
	 */
	public static final Color COLOR_BLUE = new Color(224,224,255);
	public static final Color COLOR_GREEN = new Color(224,255,224);
	public static final Color COLOR_RED = new Color(255,224,224);
	public static final Color COLOR_YELLOW = new Color(255,255,180);

	Color farbe;

	public ColorTableCellRenderer(Color col) {
		farbe = col;
	}
	
	public Component getTableCellRendererComponent(
            JTable table, Object color,
            boolean isSelected, boolean hasFocus,
            int row, int column) {
		
		if(!isSelected) setBackground(farbe);		
		
		return super.getTableCellRendererComponent(table, 
						color, isSelected, hasFocus, row, column);
	}
}
