package de.taliis.plugins.dbc.mixeditor;

import starlight.taliis.core.ZeroTerminatedString;

/**
 * Mini storage class for entrys that have a
 * complete different real value than what
 * they seems to display
 * 
 * @author ganku
 *
 */
public class MixEntry implements Comparable {
	String label;
	int value;
	
	/**
	 * @param visible -> text user can see
	 * @param value -> the real value
	 */
	public MixEntry(String visible, int value) {
		label = visible;
		this.value = value;
	}
	
	public String toString() {
		return label;
	}
	
	public int getRealValue() {
		return value;
	}


	public int compareTo(Object o) {
		return o.toString().compareTo(this.label);
	}
}
