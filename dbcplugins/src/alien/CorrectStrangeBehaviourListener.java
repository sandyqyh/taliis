package alien;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JScrollPane;
import javax.swing.JTable;



/**
      *
      * @author dbeutner 16-MAY-2002
      * See Sun Java bug report 4127936.
      * Amended 29/3/2007 by RedGrittyBrick to add
      * constructor with parameters
      */ 

public class CorrectStrangeBehaviourListener
             extends ComponentAdapter {

         private JTable table;

         private JScrollPane scrollPane;

         public CorrectStrangeBehaviourListener(JTable table,
                 JScrollPane scrollPane) {
             this.table = table;
             this.scrollPane = scrollPane;
         }

         public void componentResized(ComponentEvent e) {
             if (table.getPreferredSize().width
                     <= scrollPane.getViewport()
                            .getExtentSize().width) {
                 table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
             } else {
                 table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
             }
         }
     } 