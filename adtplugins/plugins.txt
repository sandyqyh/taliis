#	Taliis (c) 2007-2009 
#		by Melanie de Neygevisage	(Mae)
#			Tharo Herberg 		  	(ganku)
#		used libs are (c) by the contributors.

# Plugin Pack config File

pack_name = ADT related plugins and main driver
pack_info = Anything needed to work with ADT files
pack_autor = Tharo Herberg
pack_version = 1.0

plugin_count = 9

# the full classpath of the plugin
plugin_1 = de.taliis.plugins.adt.adtStorage
plugin_2 = de.taliis.plugins.adt.alphaView
plugin_3 = de.taliis.plugins.adt.shadowDel
plugin_4 = de.taliis.plugins.adt.shadowView
plugin_5 = de.taliis.plugins.adt.adtOldCheck
plugin_6 = de.taliis.plugins.adt.adtReplaceTools
plugin_7 = de.taliis.plugins.adt.adtWater
