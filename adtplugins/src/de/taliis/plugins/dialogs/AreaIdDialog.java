package de.taliis.plugins.dialogs;


import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class AreaIdDialog implements ActionListener{

	public int areaid=0,startc=0,endc=255;
	public boolean ok = false;
	JDialog frame;
	JTextField startChunk,endChunk,id;
	JButton okButton = new JButton("OK");
	
	
	public void addComponentsToPane(Container pane){
    	pane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		startChunk = new JTextField("0");
		pane.add(startChunk);
		
		endChunk = new JTextField("255");
		pane.add(endChunk);
		
		id = new JTextField("AreaID(0-4294967295)");
		pane.add(id);
		
		
		c.gridx = 6;
		c.gridy = 2;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.NONE;
		okButton.addActionListener(this);
		pane.add(okButton, c);
	
	}
	
	public AreaIdDialog(Component rel){
		//Create and set up the window.
        frame = new JDialog();
        frame.setLocationRelativeTo(rel);
        frame.setTitle("Set AreaID");
        frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
        // using this line make the Java VM crash at java 5 03 MAC .. o.O
        // static or nonstatic icon doesnt madder
        //frame.setIconImage(IconNew.getImage());
        frame.setModal(true);
        
        JPanel bla = new JPanel();

        
        //Set up the content pane.
        addComponentsToPane(bla/*frame.getContentPane()/**/);
        frame.setContentPane(bla);
        //Display the window.
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource()==okButton) {
			areaid=Integer.parseInt(id.getText());
			startc=Integer.parseInt(startChunk.getText());
			endc=Integer.parseInt(endChunk.getText());
			frame.dispose();
			ok = true;
		}
	}
	
}