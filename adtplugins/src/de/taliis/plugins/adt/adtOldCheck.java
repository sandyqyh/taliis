package de.taliis.plugins.adt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.MenuElement;

import starlight.taliis.core.files.adt;
import starlight.taliis.helpers.adtChecker;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.eventServer;


public class adtOldCheck implements Plugin, ActionListener {
	fileMananger fm;
	ImageIcon icon;
	
	// our Menu
	JMenuItem miCheck;
	JMenu menu;
	
	
	public boolean checkDependencies() { return true; }
	public ImageIcon getIcon() { return icon; }
	public int getPluginType() { return Plugin.PLUGIN_TYPE_FUNCTION; }
	public String[] getSupportedDataTypes() { return new String[] { "adt" }; }
	public String[] neededDependencies() { return null; }

	
// load our icon
	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/lightbulb.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}
	}

	public void setConfigManangerRef(configMananger arg0) { }
	public void setEventServer(eventServer arg0) { }
	public void setFileManangerRef(fileMananger arg0) { fm = arg0; }
	public void setPluginPool(Vector<Plugin> arg0) { }
	public void unload() { }

// setup menue
	public void setMenuRef(JMenuBar arg0) {
		// cast out edit menue
		for(MenuElement men : arg0.getSubElements()) {
			if(men instanceof JMenu) {
				if(((JMenu)men).getName().contains("edit_adt")) {
					menu = (JMenu)men;
					menu.setEnabled(true);
					break;
				}
			}
		}
		
		// insert our entry
		miCheck = new JMenuItem("Check Offsets");
		  miCheck.setName("meni_adt_checkoffsets");
		  miCheck.setIcon(icon);
		  miCheck.addActionListener(this);
		menu.addSeparator();
		menu.add(miCheck);
	}

// action listener
	public void actionPerformed(ActionEvent e) {
		
		// check adt file
		if(e.getSource()==miCheck) {
			openedFile os = fm.getActiveFile();
			if(os.obj instanceof adt) {
				adt f = (adt)os.obj;
				
				adtChecker check = new adtChecker(f);
				check.check();
			}
		}
	}

}
