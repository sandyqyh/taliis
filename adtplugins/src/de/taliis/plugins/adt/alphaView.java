package de.taliis.plugins.adt;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import starlight.taliis.core.chunks.adt.MCAL_Entry;
import starlight.taliis.core.files.adt;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;

/**
 * Displays adt's alphamap infomation as picture
 * 
 * @author tharo
 * 
 */
public class alphaView implements Plugin, PluginView, ActionListener {
	fileMananger fm;
	configMananger cm;
	ImageIcon icon = null;
	JMenu mAlpha;
	JMenuItem mImport, mExport;

	String dep[] = { 
			"starlight.taliis.core.files.wowfile", 
			"starlight.taliis.core.files.adt",
		};

	public boolean checkDependencies() {
		String now = "";
		try {
			for (String s : dep) {
				now = s;
				Class.forName(s);
			}
			return true;
		} catch (Exception e) {
			System.err.println("Class \"" + now + "\" not found.");
			return false;
		}
	}

	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return icon;
	}

	public int getPluginType() {
		return PLUGIN_TYPE_VIEW;
	}

	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[] { "adt" };
	}

	public String[] neededDependencies() {
		return dep;
	}

	public void setClassLoaderRef(URLClassLoader ref) {
		try {
			URL u = ref.getResource("icons/color_wheel.png");
			icon = new ImageIcon(u);
		} catch (NullPointerException e) {
		}
	}

	public void setConfigManangerRef(configMananger ref) {
		cm = ref;
	}

	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub

	}

	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
	}

	public void setMenuRef(JMenuBar ref) {
		mAlpha = new JMenu("Alpha Layer");
		mAlpha.setIcon(icon);

		mImport = new JMenuItem("Import from png ..");
		mImport.addActionListener(this);

		mExport = new JMenuItem("Export to png ..");
		mExport.addActionListener(this);

		mAlpha.add(mImport);
		mAlpha.add(mExport);

		// get our sub menue
		for (int c = 0; c < ref.getMenuCount(); c++) {
			JMenu menu = ref.getMenu(c);
			if (menu.getName().compareTo("edit_adt") == 0) {
				menu.setEnabled(true);
				menu.add(mAlpha);
				return;
			}
		}
		System.err.println("No ADT Menu found o.O");
	}

	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub

	}

	public void unload() {
		// TODO Auto-generated method stub
	}

	public JPanel createView() {
		// get file
		openedFile of = fm.getActiveFile();

		if (of == null || !(of.obj instanceof adt))
			return null;
		adt obj = (adt) of.obj;
		// create buffered image

		BufferedImage img = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_RGB);
		for(int x=0; x<16; x++) {
			for(int y=0; y<16; y++) {
				int layers = obj.mcnk[x*16+y].mcal.getLenght();
				//if(layers==0) continue;
					
				// preload data
				int data[] = new int[4096];
				
	
				for(int layer=0; layer<layers; layer++){
					MCAL_Entry mce = obj.mcnk[x*16+y].mcal.getLayerNo(layer);
					
					for(int c=0; c<4096; c++) {
						
						// color = old color | new color>>layer*16bit
						int newcolor = (mce.getValue(c)&0xff)<<(layer*8);
						
						// combined
						data[c] = data[c] | newcolor;
										
				}
					
					
					for(int c=0; c<64; c++)
						
						img.setRGB(y*64,				//int startX,
					                  x*64+c,		//int startY,
					                  64,			//int w,
					                  1,			//int h,
					                  data,			//int[] rgbArray,
					                  c*64,			//int offset,
					                  64);			//int scansize
					
					}
				}}
		// create panel, display
		Icon icon = new ImageIcon(img);
		JLabel label = new JLabel(icon);
		label.setMinimumSize(new Dimension(1024, 1024));

		JPanel f = new JPanel();
		f.add(label);

		JScrollPane scr = new JScrollPane(f, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scr.setPreferredSize(new Dimension(1024, 1024));

		JPanel r = new JPanel();
		r.setLayout(new BorderLayout());
		r.add(scr, BorderLayout.CENTER);

		return r;

	}


	private void Import() {
		// get file
		openedFile of = fm.getActiveFile();

		if (of == null || !(of.obj instanceof adt))
			return;

		// get data
		adt obj = (adt) of.obj;
		BufferedImage img = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_RGB);
		for(int TextureID=0;TextureID<obj.mtex.entrys.length;TextureID++){
    		
    		String filename = of.f.getAbsolutePath();
    		File f = new File(filename +"_"+TextureID+"_"+ ".png");
    		try {
				img=ImageIO.read(f);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
    		for(int x=0; x<16; x++) {
					for(int y=0; y<16; y++) {
						int layers = obj.mcnk[x*16+y].getNLayers();
						//if(layers==0) continue;
							
						// preload data
						int data[] = new int[4096];
						
						for(int c=0; c<64; c++)
						img.getRGB(y*64,				//int startX,
						                  x*64+c,		//int startY,
						                  64,			//int w,
						                  1,			//int h,
						                  data,			//int[] rgbArray,
						                  c*64,			//int offset,
						                  64);			//int scansize
						
						for(int layer=0; layer<layers; layer++){
							
							
							if(obj.mcnk[x*16+y].mcly.layer[layer].getTextureID()==TextureID){
								if(obj.mcnk[x*16+y].mcal.getLenght()-1>=obj.mcnk[x*16+y].mcly.layer[layer].getAlphaID())
								for(int c=0; c<4096; c++) {
									obj.mcnk[x*16+y].mcal.layers[obj.mcnk[x*16+y].mcly.layer[layer].getAlphaID()].setValue(c, data[c]);
										
								}
								//this just leads to crappy results so ignore it
								/*else
								{
									obj.mcnk[x*16+y].mcal.createLayer();
									for(int c=0; c<4096; c++) {
										obj.mcnk[x*16+y].mcal.layers[obj.mcnk[x*16+y].mcly.layer[layer].getAlphaID()].setValue(c, data[c]);
											
									}
								}*/
							}
							
							}
						}}
    		System.out.println("Importet Layer: "+TextureID);

		}	
	}

	/**
	 * Exports our layers as RGB image. Same directory as the ADT file have itself.
	 */
	private void Export() {
		// get file
		openedFile of = fm.getActiveFile();
		
		if(of==null || !(of.obj instanceof adt)) return;
		
		// get data
		adt obj = (adt)of.obj;
		
		// create buffered image
		BufferedImage img = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_RGB);
		for(int TextureID=0;TextureID<obj.mtex.entrys.length;TextureID++){
    		img = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_RGB);

    		for(int x=0; x<16; x++) {
					for(int y=0; y<16; y++) {
						int layers = obj.mcnk[x*16+y].getNLayers();
						//if(layers==0) continue;
							
						// preload data
						int data[] = new int[4096];
						
			
						for(int layer=0; layer<layers; layer++){
							
							if(obj.mcnk[x*16+y].mcal.getSize()!=0){
							if(obj.mcnk[x*16+y].mcly.layer[layer].getTextureID()==TextureID && obj.mcnk[x*16+y].mcal.getLenght()-1>=obj.mcnk[x*16+y].mcly.layer[layer].getAlphaID()){
								MCAL_Entry mce = obj.mcnk[x*16+y].mcal.getLayerNo(obj.mcnk[x*16+y].mcly.layer[layer].getAlphaID());
								for(int c=0; c<4096; c++) {

								data[c] = (mce.getValue(c)&0xff);
												
							}
							}

							
							
							
							
							for(int c=0; c<64; c++)

								img.setRGB(y*64,				//int startX,
							                  x*64+c,		//int startY,
							                  64,			//int w,
							                  1,			//int h,
							                  data,			//int[] rgbArray,
							                  c*64,			//int offset,
							                  64);			//int scansize
							
							}}
						}}
    		

    			
    	
		String filename = of.f.getAbsolutePath();
		
		File file = new File(filename +"_"+TextureID+"_"+ ".png");

        try {
			ImageIO.write(img, "png", file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		}			
		System.out.println("Alpha layer successfull exported to: " + ".png");
					
	}

	public String toString() {
		return "Texture Blending";
	}

	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == mImport)
			Import();
		else
			if (arg0.getSource() == mExport)
				Export();
	}
}
