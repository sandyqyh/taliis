package starlight.taliis.apps.editors;



import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

import starlight.alien.*;
import starlight.taliis.core.files.*;
import starlight.taliis.core.chunks.adt.MCNK;
import starlight.taliis.helpers.fileLoader;


/**
 * Some stuff which is interesting for real worldbuilders
 * @author Tigurius
 *
 */
public class adtholeFileTable extends JPanel 
implements ActionListener {
adt obj;
JTable table;
JToolBar toolBar;
JTextField newFile;

final String ADD = "add";
final String DEL = "del";

public adtholeFileTable(adt reference) {
obj = reference;
this.setLayout(new BorderLayout());


toolBar = new JToolBar();

add(toolBar, BorderLayout.PAGE_START);

table = new JTable(new adtholeTableModel(obj.mcnk));

//Create the scroll pane and add the table to it.
JScrollPane scrollPane = new JScrollPane(table);
scrollPane.addComponentListener(new
CorrectStrangeBehaviourListener(table, scrollPane)); 

table.getColumnModel().getColumn(0).setMaxWidth(20);
table.getColumnModel().getColumn(1).setMaxWidth(30);
table.getColumnModel().getColumn(3).setMaxWidth(50);

//Add the scroll pane to this panel.
add(scrollPane);
}


protected JButton makeNavigationButton(String imageName,
String actionCommand,
String toolTipText,
String altText) {
//Look for the image.
String imgLocation = "images/icons/"
+ imageName
+ ".png";

//Create and initialize the button.
JButton button = new JButton();
button.setActionCommand(actionCommand);
button.setToolTipText(toolTipText);
button.addActionListener(this);

if (imageName != "" && imageName != null) {    //image found
button.setIcon(fileLoader.createImageIcon(imgLocation));
} 
else {                                     //no image found
button.setText(altText);
System.err.println("Resource not found: " + imgLocation);
}

return button;
}


public void actionPerformed(ActionEvent e) {

}

}

class adtholeTableModel extends AbstractTableModel {
static ImageIcon page = fileLoader.createImageIcon("images/icons/holes.png");
MCNK archive[];

// Bezeichnungen
Vector columnNames = new Vector();

adtholeTableModel(MCNK[] reference) {
archive = reference;
}

public int getColumnCount() {
return 4;
}

public int getRowCount() {
return 256;
}

public String getColumnName(int col) {
if(col==1) return "Chunk";
else if(col==2) return "Holes";
else return "";
}

public Object getValueAt(int row, int col) {
if(col==0) return page;
else if(col==1) return row;
else if(col==2) return archive[row].getHoles();
else return null;
}

public Class getColumnClass(int c) {
if(c==0) return Icon.class;
else return Integer.class;
}

public boolean isCellEditable(int row, int col) {
if(col==2) return true;
else return false;
}

public void setValueAt(Object value, int row, int col) {
if(col==2) archive[row].setHoles((Integer)value);
}

public void clear() {
}
}