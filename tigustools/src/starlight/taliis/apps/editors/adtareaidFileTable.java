package starlight.taliis.apps.editors;



import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;

import de.taliis.plugins.tigusplugins.mixeditor.dbcLoader;

import starlight.alien.*;
import starlight.taliis.core.files.*;
import starlight.taliis.core.chunks.adt.MCNK;
import starlight.taliis.helpers.fileLoader;


/**
 * Some stuff which is interesting for real worldbuilders
 * @author Tigurius
 *
 */
public class adtareaidFileTable extends JPanel 
implements ActionListener {
adt obj;
JTable table;
JToolBar toolBar;
JTextField newFile;
dbcLoader dLoader;
final String ADD = "add";
final String DEL = "del";

public adtareaidFileTable(adt reference,dbcLoader dLoader) {
obj = reference;
this.dLoader=dLoader;
this.setLayout(new BorderLayout());


toolBar = new JToolBar();

add(toolBar, BorderLayout.PAGE_START);

table = new JTable(new adtareaTableModel(obj.mcnk,dLoader));
//table.setPreferredScrollableViewportSize(new Dimension(70, 70));

//Create the scroll pane and add the table to it.
JScrollPane scrollPane = new JScrollPane(table);
scrollPane.addComponentListener(new
CorrectStrangeBehaviourListener(table, scrollPane)); 

table.getColumnModel().getColumn(0).setMaxWidth(20);
table.getColumnModel().getColumn(1).setMaxWidth(30);
table.getColumnModel().getColumn(2).setMaxWidth(50);

//Add the scroll pane to this panel.
add(scrollPane);
}


protected JButton makeNavigationButton(String imageName,
String actionCommand,
String toolTipText,
String altText) {
//Look for the image.
String imgLocation = "images/icons/"
+ imageName
+ ".png";

//Create and initialize the button.
JButton button = new JButton();
button.setActionCommand(actionCommand);
button.setToolTipText(toolTipText);
button.addActionListener(this);

if (imageName != "" && imageName != null) {    //image found
button.setIcon(fileLoader.createImageIcon(imgLocation));
} 
else {                                     //no image found
button.setText(altText);
System.err.println("Resource not found: " + imgLocation);
}

return button;
}


public void actionPerformed(ActionEvent e) {

}

}

class adtareaTableModel extends AbstractTableModel {
static ImageIcon page = fileLoader.createImageIcon("images/icons/area.png");
MCNK archive[];
dbcLoader loader;
// Bezeichnungen
Vector columnNames = new Vector();

adtareaTableModel(MCNK[] reference,dbcLoader loader) {
archive = reference;
this.loader=loader;
}

public int getColumnCount() {
return 4;
}

public int getRowCount() {
return 256;
}

public String getColumnName(int col) {
if(col==1) return "Chunk";
else if(col==2) return "AreaID";
else if(col==3) return "AreaName";
else return "";
}

public Object getValueAt(int row, int col) {
if(col==0) return page;
else if(col==1) return row;
else if(col==2) return archive[row].getAreaID();
else if(col==3) return loader.getDBCValue_s("AreaTable.dbc", 0, 14, archive[row].getAreaID());
else return null;
}

public Class getColumnClass(int c) {
if(c==0) return Icon.class;
else return Integer.class;
}

public boolean isCellEditable(int row, int col) {
if(col==2) return true;
else return false;
}

public void setValueAt(Object value, int row, int col) {
if(col==2) archive[row].setAreaID((Integer)value);
}

public void clear() {
}
}