package de.taliis.plugins.tigusplugins;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.JPanel;


import starlight.taliis.apps.editors.adtareaidFileTable;
import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.wowfile;
import starlight.taliis.helpers.fileLoader;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;
import de.taliis.plugins.tigusplugins.mixeditor.dbcLoader;

public class adtAreaIDAdder implements Plugin, PluginView {
	ImageIcon viewIcon = null;
	fileMananger fm;
	dbcLoader dLoader;
	Vector<Plugin> pp;
	
	String dep[] = {
			"starlight.taliis.core.files.wowfile",
			"starlight.taliis.core.files.adt",
			"starlight.alien.CorrectStrangeBehaviourListener"
		};
	
	public boolean checkDependencies() {
		String now = "";
		try {
			for(String s : dep) {
				now = s;
				Class.forName(s);
			}
			return true;
		} catch(Exception e) {
			System.err.println("Class \"" + now + "\" not found.");
			return false;
		}
	}
	
	public ImageIcon getIcon() {
		return viewIcon;
	}
	
	public int getPluginType() {
		return PLUGIN_TYPE_VIEW;
	}
	
	public String[] getSupportedDataTypes() {
		return new String[] {"adt"};
	}
	
	public String[] neededDependencies() {
		return dep;
	}
	
	public void setClassLoaderRef(URLClassLoader ref) {
		fileLoader.cl = ref;
		try {
			URL u = ref.getResource("icons/area.png");
			viewIcon = new ImageIcon( u );
		} catch (Exception e) {};/**/
	}

	@Override
	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFileManangerRef(fileMananger ref) {
		fm = ref;
		
	}

	@Override
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		pp = ref;
	}

	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}
	
	public String toString() {
		return "AreaIDs";
	}

	@Override
	public JPanel createView() {
		wowfile f = (wowfile)fm.getActiveFile().obj;
		for(Plugin p : pp) {
			if(p.getClass().toString().endsWith("mpqFileRequest"))
				dLoader=new dbcLoader(fm, p);	
		}
		
		if(f instanceof adt) {
			return new adtareaidFileTable ( (adt)f ,dLoader);
		}
		return null;
	}
}