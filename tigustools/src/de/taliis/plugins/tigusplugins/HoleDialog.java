package de.taliis.plugins.tigusplugins;


import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;




public class HoleDialog implements ActionListener{


	public boolean ok = false;
	public int holeval=0;
	JDialog frame;

	JCheckBox j[];
	int bitfield[]={
			0x1		,0x2	,0x4	,0x8
			,0x10 	,0x20 	,0x40 	,0x80
			,0x100 	,0x200 	,0x400 	,0x800
			,0x1000 ,0x2000 ,0x4000 ,0x8000
			};
	JButton okButton = new JButton("OK");
	
	
	public void addComponentsToPane(Container pane){
    	pane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		int x=0,y=0;

		int k=0;
		boolean zeile=false;
		for(int i=0;i<16;i++){
			if(k==0)x=0;
			if(k>0)x+=4;
			if(zeile==true){y+=4;zeile=false;}
			if(k==3)zeile=true;
			if(k<3)k++;else	k=0;
			c.gridx=x;
			c.gridy=y;

			pane.add(j[i],c);
		}
		
		x+=4;
		y+=4;
		c.gridx = x;
		c.gridy = y;
		c.gridwidth = 2;
		c.fill = GridBagConstraints.NONE;
		okButton.addActionListener(this);
		pane.add(okButton, c);
	
	}
	
	public HoleDialog(Component rel,int holes){
		//Create and set up the window.
        frame = new JDialog();
        frame.setLocationRelativeTo(rel);
        frame.setTitle("Set Holes");
        frame.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        
        // using this line make the Java VM crash at java 5 03 MAC .. o.O
        // static or nonstatic icon doesnt madder
        //frame.setIconImage(IconNew.getImage());
        frame.setModal(true);
		j=new JCheckBox[16];
		for(int i=0;i<16;i++){
			j[i]=new JCheckBox();
			if((holes&bitfield[i])!=0){
				j[i].setSelected(true);
			}
		}
        JPanel bla = new JPanel();

        
        //Set up the content pane.
        addComponentsToPane(bla/*frame.getContentPane()/**/);
        frame.setContentPane(bla);
        //Display the window.
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource()==okButton) {
			for(int i=0;i<16;i++){
				if(j[i].isSelected()==true){
					holeval+=bitfield[i];
				}
			}
			frame.dispose();
			ok = true;
		}
	}
	
}