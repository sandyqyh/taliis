package de.taliis.editor;

import java.util.HashMap;
import java.util.Vector;

import javax.swing.JTree;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeModel;

import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginStorage;
import de.taliis.editor.plugin.PluginView;

/**
 * Serves all opened files + the given view possibilitys
 * to the navigation tree.
 * 
 * @author Mae
 *
 */

public class fileTreeModel implements TreeModel {
	fileMananger fm;
	configMananger cm;
	
	Vector<Plugin> ppool;
	String root = "/";
	boolean rooted = true;		// rooted tells that we need to check our plugins 
								// (tree got updated)
	HashMap views;
	HashMap storages;
	
	public fileTreeModel(fileMananger f, configMananger c, Vector<Plugin> pp) {
		fm = f;
		cm = c;
		ppool = pp;
	}
	
	/**
	 * Counts the number of views we have
	 */
	private void countViews() {
		views = new HashMap();
		storages = new HashMap();
		String wesupport = "";
		
		if(ppool==null) return;
		
		// eraubte datentypen speichern, strukturen schaffen
		for(Plugin p : ppool) {
			if((p.getPluginType() & p.PLUGIN_TYPE_VIEW)!=0) {
				for(String s : p.getSupportedDataTypes()) {
					Vector<Plugin> tmp;
					if(views.get(s)==null ) {
						tmp = new Vector<Plugin>();
						tmp.add(p);
						views.put(s, tmp);
					}
					else {
						tmp = (Vector<Plugin>)views.get(s);
						tmp.add(p);
					}
				}
			}
		}
		rooted = false;
	}
	
	
	
	public Object getChild(Object parent, int index) {
		// its the root -> get file 'index'
		if(parent==root) {
			// open file
			openedFile of = null;
			// we display all files
			if(cm.getGlobalConfig().containsKey("taliis_showAllFiles"))
				of = fm.getFile(index);
			// we only count visible files
			else {
				int c = 0;
				for(openedFile o : fm.getFileList()) {
					if(o.getFileOpener()==o.OPENED_BY_USER) {
						if(c==index) {
							of = o;
							break;
						}
						else c++;
					}
				}
			}
			if(of==null) return null;
			
			String name = of.getExtension();
			
			// visible?
			if(of.getFileOpener()!=openedFile.OPENED_BY_USER)
				if(!cm.getGlobalConfig().containsKey("taliis_showAllFiles"))
					return null;
			
			// its an opened file. look for its icon and bind
			//		a menue node
			for(Plugin p : ppool) {
				if(p instanceof PluginStorage) {
					for(String s : p.getSupportedDataTypes()) {
						if(name.toLowerCase().endsWith(s)) {
							return new menuNode(p, of);
						}
					}
				}
			}
		}
		
		// its a file -> get view 'index'
		if(parent instanceof menuNode) {
			openedFile of = ((menuNode)parent).file;
			
			Vector<Plugin> tmp = (Vector<Plugin>)views.get(of.getExtension());
			if(tmp!=null) {
				return new menuNode(tmp.get(index), of);
			}
		}
		return null;
	}

	
	public int getChildCount(Object parent) {
		if(rooted==true) countViews();
		
		// files
		if(parent==root) {
			// display all files?
			if(cm.getGlobalConfig().containsKey("taliis_showAllFiles"))
				return fm.getFileList().size();
			
			
			// no. only display users files
			int c = 0;
			for(openedFile o : fm.getFileList())
				if(o.getFileOpener()==o.OPENED_BY_USER) c++;
			
			return c;
		}
		// views
		else if(parent instanceof menuNode) {
			Plugin p = ((menuNode)parent).view;

			if(p instanceof PluginStorage) {
				openedFile f = ((menuNode)parent).file;
				Vector<Plugin> tmp = (Vector<Plugin>)views.get(f.getExtension());
				if(tmp!=null)
					return tmp.size();
			}
		}

		return 0;
	}

	
	public Object getRoot() {
		rooted = true;
	    return root;
	}

	
	public boolean isLeaf(Object node) {
		if(node==root) return false;
		else if(node instanceof menuNode) {
			Plugin p = ((menuNode)node).view;
			if(p instanceof PluginStorage)return false;
			else return true;
		}
		return true;
	} 

	 public int getIndexOfChild( Object parent, Object child ) { return 0; } 
	 public void removeTreeModelListener( TreeModelListener l ) { } 
	 public void addTreeModelListener( TreeModelListener l ) { } 
	 public void valueForPathChanged( TreePath path, Object newValue ) { }
}