package de.taliis.editor;

import java.io.File;

import starlight.taliis.core.files.wowfile;

/**
 * Just a stupid storage class
 */

public class openedFile {
	public File f;
	public Object obj;
	
	private String ext;
	private boolean changed = false;
	private int openedBy = 0;
	
	public static final int OPENED_BY_USER = 0;
	public static final int OPENED_BY_PLUGIN = 1;
	public static final int OPENED_BY_EDITOR = 2;
	
	/**
	 * Just open the given file.
	 * (object will not get init.)
	 * @param file
	 */
	public openedFile (File file) {
		f = file;
		obj = null;
		
		int tmp = f.getName().lastIndexOf(".");
		ext = f.getName().substring(tmp+1);
	}
	
	/**
	 * Binds an object to a file reference
	 * @param o
	 * @param fil
	 */
	public openedFile(Object o, File fil) {
		obj = o;
		f = fil;
		
		if(f!=null) {
			int tmp = f.getName().lastIndexOf(".");
			ext = f.getName().substring(tmp+1);
		}
		else {
			int tmp = obj.getClass().toString().lastIndexOf(".");
			ext = obj.getClass().toString().substring(tmp+1);
		}
	}
	
	public String toString() {
		return f.getName();
	}
	public String getExtension() {
		return ext;
	}
	
	/**
	 * Got this file changed/edited?
	 * @return >0 if changes were made
	 */
	public int gotChanged() {
		if(changed==true) return 1;
		else return 0;
	}
	/**
	 * Sets the "got changed" flag
	 * File opener will change to "user"
	 */
	public void doChange() {
		changed = true;
		openedBy = OPENED_BY_USER;
	}
	
	public void setFileOpener(int mode) {
		openedBy = mode;
	}
	public int getFileOpener() {
		return openedBy;
	}
}