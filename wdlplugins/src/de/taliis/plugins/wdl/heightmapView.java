package de.taliis.plugins.wdl;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InvalidClassException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.wdl.MARE;
import starlight.taliis.core.files.wdl;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;



public class heightmapView implements Plugin, PluginView, ActionListener {

	fileMananger fm;
	configMananger cm;
	ImageIcon icon = null;
	JMenu mHeight;
	JMenuItem mExport;
	String dep[] = { 
			"starlight.taliis.core.files.wowfile", 
			"starlight.taliis.core.files.wdl",
		};
	
	@Override
	public boolean checkDependencies() {
		// TODO Auto-generated method stub
		String now = "";
		try {
			for (String s : dep) {
				now = s;
				Class.forName(s);
			}
			return true;
		} catch (Exception e) {
			System.err.println("Class \"" + now + "\" not found.");
			return false;
		}
	}

	@Override
	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return icon;
	}

	@Override
	public int getPluginType() {
		// TODO Auto-generated method stub
		return PLUGIN_TYPE_VIEW;
	}

	@Override
	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[] { "wdl" };
	}

	@Override
	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return dep;
	}

	@Override
	public void setClassLoaderRef(URLClassLoader ref) {
		// TODO Auto-generated method stub
		try {
			URL u = ref.getResource("icons/world_color.png");
			icon = new ImageIcon(u);
		} catch (NullPointerException e) {
		}
	}

	@Override
	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		cm = ref;
	}

	@Override
	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFileManangerRef(fileMananger ref) {
		// TODO Auto-generated method stub
		fm = ref;
	}

	@Override
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub
		mHeight = new JMenu("Heightmap");
		mHeight.setIcon(icon);
		mExport = new JMenuItem("Export to png ..");
		mExport.addActionListener(this);
		mHeight.add(mExport);
		for (int c = 0; c < ref.getMenuCount(); c++) {
			JMenu menu = ref.getMenu(c);
			if (menu.getName().compareTo("edit_wdl") == 0) {
				menu.setEnabled(true);
				menu.add(mHeight);
				return;
			}
		}
		System.err.println("No WDL Menu found o.O");
		
	}

	@Override
	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public JPanel createView() throws InvalidClassException,
			ChunkNotFoundException {
		// get file
		openedFile of = fm.getActiveFile();

		if (of == null || !(of.obj instanceof wdl))
			return null;
		wdl obj = (wdl) of.obj;
		// create buffered image

		BufferedImage img = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_RGB);
		for(int x=0; x<64; x++) {
			for(int y=0; y<64; y++) {
				if(obj.mare[x*64+y]!=null){
				MARE mare=obj.mare[x*64+y];
				// preload data
				int data[] = new int[16*16];
	
				for(int i=0; i<16*16; i++){
					data[i]=0xffffff-mare.innerheight[i];	
					
				}
					for(int c=0; c<16; c++)
						img.setRGB(y*16,				//int startX,
					                  x*16+c,		//int startY,
					                  16,			//int w,
					                  1,			//int h,
					                  data,			//int[] rgbArray,
					                  c*16,			//int offset,
					                  16);			//int scansize
					
					
				}
				else{
					int data[] = new int[16*16];
					
					for(int i=0; i<16*16; i++){
							data[i]=0;						
					}
						for(int c=0; c<16; c++)
							img.setRGB(y*16,				//int startX,
						                  x*16+c,		//int startY,
						                  16,			//int w,
						                  1,			//int h,
						                  data,			//int[] rgbArray,
						                  c*16,			//int offset,
						                  16);			//int scansize
						
						
					
					
				}
					
				}
			}
		// create panel, display
		Icon icon = new ImageIcon(img);
		JLabel label = new JLabel(icon);
		label.setMinimumSize(new Dimension(1024, 1024));

		JPanel f = new JPanel();
		f.add(label);

		JScrollPane scr = new JScrollPane(f, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scr.setPreferredSize(new Dimension(1024, 1024));

		JPanel r = new JPanel();
		r.setLayout(new BorderLayout());
		r.add(scr, BorderLayout.CENTER);

		return r;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if (arg0.getSource() == mExport)
			Export();
	}
	
	public String toString() {
		return "Heightmap Preview";
	}
	
	/**
	 * Exports our layers as RGB image. Same directory as the ADT file have itself.
	 */
	private void Export() {
		// get file
		openedFile of = fm.getActiveFile();
		
		if(of==null || !(of.obj instanceof wdl)) return;
		
		// get data
		wdl obj = (wdl)of.obj;
		
		// create buffered image
		BufferedImage img = new BufferedImage(1024, 1024, BufferedImage.TYPE_INT_RGB);
		for(int x=0; x<64; x++) {
			for(int y=0; y<64; y++) {
				
				if(obj.maof.getOffset(x, y)!=0){
				MARE mare=obj.mare[x*64+y];
				// preload data
				int data[] = new int[16*16];
	
				for(int i=0; i<16*16; i++){
					data[i]=0xffffff-mare.innerheight[i];	
					
				}
					for(int c=0; c<16; c++)
						img.setRGB(y*16,				//int startX,
					                  x*16+c,		//int startY,
					                  16,			//int w,
					                  1,			//int h,
					                  data,			//int[] rgbArray,
					                  c*16,			//int offset,
					                  16);			//int scansize
					
					
				}
				else{
					int data[] = new int[16*16];
					
					for(int i=0; i<16*16; i++){
							data[i]=0;						
					}
						for(int c=0; c<16; c++)
							img.setRGB(y*16,				//int startX,
						                  x*16+c,		//int startY,
						                  16,			//int w,
						                  1,			//int h,
						                  data,			//int[] rgbArray,
						                  c*16,			//int offset,
						                  16);			//int scansize
						
						
					
					
				}
					
				}
			}
				// set pixels
				
		// create filename
		String filename = of.f.getAbsolutePath();
		File file = new File(filename + ".png");

        try {
			ImageIO.write(img, "png", file);
		} catch (IOException e) {
			e.printStackTrace();
		}
					
		System.out.println("WDL Heightmap successfull exported to: " + filename+".png");
					
	}
	
}