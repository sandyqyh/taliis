package de.taliis.plugins.wdl;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.MenuElement;

import starlight.taliis.core.chunks.wdl.MARE;
import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.wdl;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.eventServer;



public class heightmapInjector implements Plugin, ActionListener {
	fileMananger fm;
	ImageIcon iconRep;
	
	JMenuItem miInjectHeight;
	JMenu menu;
	
	@Override
	public boolean checkDependencies() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return iconRep;
	}

	@Override
	public int getPluginType() {
		// TODO Auto-generated method stub
		return Plugin.PLUGIN_TYPE_FUNCTION;
	}

	@Override
	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[] { "wdl" };
	}

	@Override
	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setClassLoaderRef(URLClassLoader ref) {
		// TODO Auto-generated method stub
		URL u = ref.getResource("icons/photos.png");
		iconRep = new ImageIcon( u );
	}

	@Override
	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFileManangerRef(fileMananger ref) {
		// TODO Auto-generated method stub
		fm = ref;
	}

	@Override
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub
		for(MenuElement men : ref.getSubElements()) {
			if(men instanceof JMenu) {
				if(((JMenu)men).getName().contains("edit_wdl")) {
					menu = (JMenu)men;
					menu.setEnabled(true);
					break;
				}
			}
		}

		 miInjectHeight = new JMenuItem("Inject Height from ADT");
		  miInjectHeight.setIcon(iconRep);
		  miInjectHeight.addActionListener(this);
		  menu.add(miInjectHeight);
	}

	@Override
	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		wdl obj;
		openedFile os = fm.getActiveFile();

		if(os.obj instanceof wdl) {
			obj = (wdl)os.obj;	
		}
		else return;
		
		if(e.getSource()==miInjectHeight) {
			Vector<openedFile> choices = fm.getFileList();
			openedFile result = (openedFile) JOptionPane.showInputDialog(
				miInjectHeight.getComponent(),
                "Chose a Source File:\n",
                "Inject",
                JOptionPane.PLAIN_MESSAGE,
                null,
                choices.toArray(),
                null);
			
			if(result==null) return;
			if(result.obj instanceof adt) {
				adt injObj = (adt) result.obj;
				short heightmap[]=new short[16*16];
				for(int c=0; c<256; c++) {
					float temp = 0;
					for(int i=0;i<145;i++){
						temp+=injObj.mcnk[c].mcvt.getVal(i)+injObj.mcnk[c].getPosZ();
					}
					temp=temp/145;
					heightmap[c]=(short)(0x00ff-temp);
				}	
				int posx,posy;
				posy=Integer.parseInt(result.f.getAbsolutePath().substring(result.f.getAbsolutePath().length()-6, result.f.getAbsolutePath().length()-4));
				posx=Integer.parseInt(result.f.getAbsolutePath().substring(result.f.getAbsolutePath().length()-9, result.f.getAbsolutePath().length()-7));
				if(obj.mare[posy*64+posx]==null)obj.mare[posy*64+posx]=new MARE();
				for(int i=0;i<16*16;i++){
					obj.mare[posy*64+posx].setInnerHeight(i,heightmap[i]);
				}
				
				//this sucks but I'm too lazy atm to find a better way
				for(int i=0;i<17*17;i++){
					if(i<17)
						obj.mare[posy*64+posx].setOuterHeight(i, heightmap[i]);
					else if(i<(17*17)-17){
						obj.mare[posy*64+posx].setOuterHeight(i, heightmap[i-17]);
					}
					else
						obj.mare[posy*64+posx].setOuterHeight(i, heightmap[(16*16)-1]);
				}
				
			}
		}
	}
}