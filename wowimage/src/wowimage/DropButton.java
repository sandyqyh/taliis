package wowimage;
// FrontEnd Plus GUI for JAD
// DeCompiled : DropButton.class

import java.awt.Color;
import java.awt.Dimension;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextArea;

public class DropButton extends JButton
    implements ActionListener
{
    class DTListener
        implements DropTargetListener
    {

        private boolean isDragOk(DropTargetDragEvent droptargetdragevent)
        {
            return droptargetdragevent.isDataFlavorSupported(DataFlavor.javaFileListFlavor);
        }

        public void dragEnter(DropTargetDragEvent droptargetdragevent)
        {
            if(!isDragOk(droptargetdragevent))
            {
                showAcceptance(false);
                droptargetdragevent.rejectDrag();
                return;
            } else
            {
                showAcceptance(true);
                droptargetdragevent.acceptDrag(droptargetdragevent.getDropAction());
                return;
            }
        }

        public void dragOver(DropTargetDragEvent droptargetdragevent)
        {
            if(!isDragOk(droptargetdragevent))
            {
                showAcceptance(false);
                droptargetdragevent.rejectDrag();
                return;
            } else
            {
                droptargetdragevent.acceptDrag(droptargetdragevent.getDropAction());
                return;
            }
        }

        public void dropActionChanged(DropTargetDragEvent droptargetdragevent)
        {
            droptargetdragevent.acceptDrag(droptargetdragevent.getDropAction());
        }

        public void dragExit(DropTargetEvent droptargetevent)
        {
            showAcceptance(false);
        }

        public void drop(DropTargetDropEvent droptargetdropevent)
        {
            if(!droptargetdropevent.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
            {
                droptargetdropevent.rejectDrop();
                return;
            }
            Object obj = null;
            try
            {
                droptargetdropevent.acceptDrop(1);
                obj = droptargetdropevent.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                if(obj == null)
                    throw new NullPointerException();
            }
            catch(Exception exception)
            {
                exception.printStackTrace();
                droptargetdropevent.dropComplete(false);
                showAcceptance(false);
                return;
            }
            if(obj instanceof java.util.List)
            {
                try
                {
                    processFiles((java.util.List)obj);
                }
                catch(Exception exception1)
                {
                    exception1.printStackTrace();
                    droptargetdropevent.dropComplete(false);
                    showAcceptance(false);
                    return;
                }
            } else
            {
                droptargetdropevent.dropComplete(false);
                showAcceptance(false);
                return;
            }
            droptargetdropevent.dropComplete(true);
            showAcceptance(false);
        }

        DTListener()
        {
        }
    }


    private DropTarget dropTarget;
    private DropTargetListener dtListener;
    private Color defaultBackground;
    private Color activeBackground;
    private JTextArea statusText;
    public static int DECODER = 1;
    public static int ENCODER = 2;
    private int type;

    public DropButton(int i)
        throws Exception
    {
        type = i;
        if(type == DECODER)
        {
            ImageIcon imageicon = createImageIcon("icon1.png");
            setIcon(imageicon);
            setText("Decode");
            setVerticalTextPosition(3);
            setHorizontalTextPosition(0);
            setToolTipText("Drop BLP files here to decode to PNG");
        } else
        if(type == ENCODER)
        {
            ImageIcon imageicon1 = createImageIcon("icon2.png");
            setIcon(imageicon1);
            setText("Encode");
            setVerticalTextPosition(3);
            setHorizontalTextPosition(0);
            setToolTipText("Drop PNG files here to encode to BLP");
        } else
        {
            throw new Exception("invalid type");
        }
        setBorder(BorderFactory.createBevelBorder(0));
        setPreferredSize(new Dimension(200, 100));
        addActionListener(this);
        defaultBackground = getBackground();
        dtListener = new DTListener();
        dropTarget = new DropTarget(this, dtListener);
        activeBackground = new Color(0, 150, 0);
    }

    public void setStatusControl(JTextArea jtextarea)
    {
        statusText = jtextarea;
    }

    private static ImageIcon createImageIcon(String s)
    {
        java.net.URL url = (DropButton.class).getResource(s);
        if(url != null)
        {
            return new ImageIcon(url);
        } else
        {
            System.err.println("Couldn't find file: " + s);
            return null;
        }
    }

    private void showAcceptance(boolean flag)
    {
        if(flag)
            setBackground(activeBackground);
        else
            setBackground(defaultBackground);
    }

    void processFiles(java.util.List list)
    {
        statusText.setText("");
        if(type == DECODER)
        {
            DecoderThread decoderthread = new DecoderThread(list, statusText);
            decoderthread.run();
        } else
        if(type == ENCODER)
        {
            EncoderThread encoderthread = new EncoderThread(list, statusText);
            encoderthread.run();
        }
    }

    public void actionPerformed(ActionEvent actionevent)
    {
    }


}
