package wowimage;
// FrontEnd Plus GUI for JAD
// DeCompiled : DecoderThread.class

import java.io.*;
import java.util.*;
import javax.swing.JTextArea;
import wowimage.BLPDecoder;
import wowimage.ConversionException;

public class DecoderThread extends Thread
{

    List files;
    JTextArea statusText;

    public DecoderThread(List list, JTextArea jtextarea)
    {
        files = list;
        statusText = jtextarea;
    }

    public void run()
    {
        try
        {
            for(ListIterator listiterator = files.listIterator(); listiterator.hasNext();)
            {
                File file = (File)listiterator.next();
                String s = file.getAbsolutePath();
                String s1 = file.getName();
                StringTokenizer stringtokenizer = new StringTokenizer(s1, ".");
                String s2 = stringtokenizer.nextToken() + ".png";
                String s3 = file.getParent() + "/" + s2;
                try
                {
                    BLPDecoder blpdecoder = new BLPDecoder(s);
                    blpdecoder.writePNG(s3);
                    statusText.append("Decoded " + s1 + " into a .png\n");
                }
                catch(ConversionException conversionexception)
                {
                    statusText.append("Couldn't decode " + s1 + "\n");
                    statusText.append(conversionexception.getMessage() + "\n");
                }
                catch(IOException ioexception1)
                {
                    statusText.append("Couldn't write to " + s3 + "\n");
                    statusText.append("File may be locked by another program\n");
                }
            }

        }
        catch(RuntimeException runtimeexception)
        {
            statusText.append("Critical Error\n");
            statusText.append(runtimeexception.toString() + "\n");
            statusText.append("Log written to error.txt\n");
            try
            {
                runtimeexception.printStackTrace(new PrintStream(new FileOutputStream("error.txt")));
            }
            catch(IOException ioexception)
            {
                statusText.append("Critical Error writing error log file");
            }
            runtimeexception.printStackTrace();
        }
    }
}
