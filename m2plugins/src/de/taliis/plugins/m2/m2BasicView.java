package de.taliis.plugins.m2;


import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import starlight.taliis.core.files.m2;
import starlight.taliis.core.files.wdt;
import starlight.taliis.core.files.wowfile;

import de.taliis.plugins.m2.*;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.eventServer;



public class m2BasicView implements Plugin, PluginView, ActionListener {
	ImageIcon icon = null;
	fileMananger fm = null;
	JTable table;
	@Override
	public boolean checkDependencies() {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return icon;
	}
	@Override
	public int getPluginType() {
		// TODO Auto-generated method stub
		return this.PLUGIN_TYPE_VIEW;
	}
	@Override
	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[]{ "m2" }; 
	}
	@Override
	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setClassLoaderRef(URLClassLoader ref) {
		// TODO Auto-generated method stub
		try {
			URL u = ref.getResource("icons/shading.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}		
	}
	@Override
	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		String key = "taliis_storage_defaultView_m2";
		if(!ref.getGlobalConfig().containsKey(key)) {
			ref.getGlobalConfig().setProperty(
					key, "de.taliis.plugins.m2.m2BasicView"
				);
		}	
		
	}
	@Override
	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setFileManangerRef(fileMananger ref) {
		// TODO Auto-generated method stub
		fm = ref;	
	}
	@Override
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub

	}
	@Override
	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public JPanel createView() {
		
		// TODO Auto-generated method stub
		JPanel main = new JPanel();
		openedFile of = fm.getActiveFile();
		wowfile f = (wowfile)fm.getActiveFile().obj;
		if(of.obj instanceof m2) {
			main.setLayout(new BorderLayout());
		}
		if(f instanceof m2){
			return new BaseInfoTable((m2)f);
		}
		return main;
	}
	public String toString(){
		return "Some Informations";
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}

/**
 * Shitti tables hates scrolling ...
 * @author ganku
 */
class CorrectStrangeBehaviourListener
	extends ComponentAdapter {
	
	private JTable table;
	
	private JScrollPane scrollPane;
	
	public CorrectStrangeBehaviourListener(JTable table,
	    JScrollPane scrollPane) {
	this.table = table;
	this.scrollPane = scrollPane;
	}
	
	public void componentResized(ComponentEvent e) {
	if (table.getPreferredSize().width
	        <= scrollPane.getViewport()
	               .getExtentSize().width) {
	    table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
	} else {
	    table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	}
	}
} 