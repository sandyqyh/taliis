package de.taliis.plugins.m2;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import starlight.alien.*;
import starlight.taliis.apps.editors.AdvJTableCell;
import starlight.taliis.apps.editors.TableColorRenderer;
import starlight.taliis.core.files.m2;
import starlight.taliis.helpers.fileLoader;

public class BaseInfoTable extends JPanel 
implements ActionListener {
	
	m2 obj;
	JTable table;
	JToolBar toolBar;
	JTextField newFile;
	


	public BaseInfoTable(m2 reference) {
		obj = reference;
		this.setLayout(new BorderLayout());
	
		
		
        table = new JTable(new BaseInfoTableModel(obj));
        //table.setPreferredScrollableViewportSize(new Dimension(70, 70));

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.addComponentListener(new
                CorrectStrangeBehaviourListener(table, scrollPane)); 

        table.getColumnModel().getColumn(0).setMaxWidth(20);


        //Add the scroll pane to this panel.
        add(scrollPane, "Center");
	}
	
	protected JButton makeNavigationButton(String imageName,
            String actionCommand,
            String toolTipText,
            String altText) {
		//Look for the image.
		String imgLocation = "images/icons/"
		+ imageName
		+ ".png";

		//Create and initialize the button.
		JButton button = new JButton();
		button.setActionCommand(actionCommand);
		button.setToolTipText(toolTipText);
		button.addActionListener(this);
		
		if (imageName != "" && imageName != null) {    //image found
			button.setIcon(fileLoader.createImageIcon(imgLocation));
		}
		else {                                     //no image found
			button.setText(altText);
			System.err.println("Resource not found: " + imgLocation);
		}

		return button;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        

	}
	
}

class BaseInfoTableModel extends AbstractTableModel {
	static ImageIcon page = fileLoader.createImageIcon("images/icons/page.png");
	
	m2 obj;
	
	// Bezeichnungen
    Vector columnNames = new Vector();
    
    BaseInfoTableModel(m2 aspect) {
    	obj	=	aspect;
    }
    
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 3;
	}
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return 7;
	}
	
    public String getColumnName(int col) {
        if(col==1) return "Description";
        else if(col==2) return "Offset";
        else return "";
    }
	
	@Override
	public Object getValueAt(int row, int col) {
		// TODO Auto-generated method stub
    	if(col==0) return page;
    	else if(col==1 && row==0) return "Number of Cameras";
    	else if(col==2 && row==0) return obj.header.getnCameras();
    	else if(col==1 && row==1) return "Offset Camerabloc";
    	else if(col==2 && row==1) return obj.header.getofsCameras();
    	else if(col==1 && row==2) return "Number of Texture";
    	else if(col==2 && row==2) return obj.header.getnTextures();
    	else if(col==1 && row==3) return "Offset Textureblock";
    	else if(col==2 && row==3) return obj.header.getofsTextures();
    	else if(col==1 && row==4) return "Number of Transparency";
    	else if(col==2 && row==4) return obj.header.getnTransparency();
    	else if(col==1 && row==5) return "Offset Transparencyblock";
    	else if(col==2 && row==5) return obj.header.getofsTransparency();
    	else if(col==1 && row==6) return "Transparency";
    	else if(col==2 && row==6){ if(obj.header.getnTransparency()!=0)return obj.getTransparency(0)[0];
    							else return "0";
    							}
    	else return null;
	}
	
    public Class getColumnClass(int c) {
        if(c==0) return Icon.class;
        else return String.class;
    }
    
    public boolean isCellEditable(int row, int col) {
    	if(col==2 && row==6 && obj.header.getnTransparency()!=0) return true;
    	else return false;
    }
    
    public void setValueAt(Object value, int row, int col) {
    	short val=0;
	    try {
	    	switch(col) {
	    		case 2:
	    			// integer value
	    			val = Short.valueOf(value.toString());
	    			break;

	    		default:
	    			return;
	    	}
	    } catch(Exception exp) {
	    	return;
	    }
    	if(col==2 && row==6) obj.setTransparency(val,0) ;
    }
	
}
	