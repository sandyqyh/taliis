package de.taliis.plugins.m2.texture;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import starlight.taliis.core.files.m2;
import starlight.taliis.core.files.wowfile;
import starlight.taliis.helpers.fileLoader;

import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginView;
import de.taliis.editor.plugin.eventServer;
import de.taliis.plugins.m2.texture.m2TextureTable;

public class m2TextureView implements Plugin, PluginView {
	ImageIcon viewIcon = null;
	fileMananger fm;
	
	String dep[] = {
			"starlight.taliis.core.files.wowfile",
			"starlight.taliis.core.files.m2",
			"starlight.alien.CorrectStrangeBehaviourListener"
		};
	
	@Override
	public boolean checkDependencies() {
		// TODO Auto-generated method stub
		String now = "";
		try {
			for(String s : dep) {
				now = s;
				Class.forName(s);
			}
			return true;
		} catch(Exception e) {
			System.err.println("Class \"" + now + "\" not found.");
			return false;
		}
	}
	@Override
	public ImageIcon getIcon() {
		// TODO Auto-generated method stub
		return viewIcon;
	}
	@Override
	public int getPluginType() {
		// TODO Auto-generated method stub
		return PLUGIN_TYPE_VIEW;
	}
	@Override
	public String[] getSupportedDataTypes() {
		// TODO Auto-generated method stub
		return new String[] {"m2"};
	}
	@Override
	public String[] neededDependencies() {
		// TODO Auto-generated method stub
		return dep;
	}
	@Override
	public void setClassLoaderRef(URLClassLoader ref) {
		// TODO Auto-generated method stub
		fileLoader.cl = ref;
		try {
			URL u = ref.getResource("icons/shading.png");
			viewIcon = new ImageIcon( u );
		} catch(NullPointerException e) {}	
	}
	@Override
	public void setConfigManangerRef(configMananger ref) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setEventServer(eventServer ref) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setFileManangerRef(fileMananger ref) {
		fm=ref;
		
	}
	@Override
	public void setMenuRef(JMenuBar ref) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void setPluginPool(Vector<Plugin> ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public JPanel createView() {
		// TODO Auto-generated method stub
		wowfile f = (wowfile)fm.getActiveFile().obj;
		if(f instanceof m2) {
			return new m2TextureTable ( (m2)f );
		}
		return null;
	}
	
	public String toString() {
		return "Texture Files";
	}
	
	@Override
	public void unload() {
		// TODO Auto-generated method stub
		
	}
	
}