package de.taliis.plugins.storage;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;
import javax.swing.filechooser.FileFilter;

import starlight.taliis.core.files.adt;
import starlight.taliis.core.files.wdt;
import starlight.taliis.core.files.wowfile;
import starlight.taliis.helpers.adtCoordHelper;
import starlight.taliis.helpers.fileLoader;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginStorage;
import de.taliis.editor.plugin.eventServer;
import de.taliis.plugins.dialogs.newWDTDialog;

/**
 * Implements the core wdt functionality whitch is 
 * atm not very big but .. whatever. It is something!
 * 
 * @author ganku
 *
 */
public class wdtStorage implements Plugin, PluginStorage {
	ImageIcon icon = null;
	fileMananger fm;
	eventServer es;
	
	public boolean checkDependencies() { return true; }
	public ImageIcon getIcon() { return icon; }
	
	public void setEventServer(eventServer ref) { 
		es = ref;
	}
	
	public int getPluginType() {
		return this.PLUGIN_TYPE_STORAGE;
	}

	public String[] getSupportedDataTypes() {
		return new String[]{ "wdt" }; 
	}

	public String[] neededDependencies() {
		return null;
	}

	public void setClassLoaderRef(URLClassLoader ref) { 
		try {
			URL u = ref.getResource("icons/shading.png");
			icon = new ImageIcon( u );
		} catch(NullPointerException e) {}		
	}

	public void setConfigManangerRef(configMananger arg0) {	}
	public void setFileManangerRef(fileMananger ref) {fm = ref;	}
	public void setMenuRef(JMenuBar arg0) {	}
	public void setPluginPool(Vector<Plugin> arg0) { }
	public void unload() { }
	
	public wowfile create() {	
		newWDTDialog n = new newWDTDialog( null );
	if(n.ok==true) {
		wowfile tmp = new wdt();
		
		
		// register by ourselfes
		fm.registerObject(
			tmp, 
			new File(n.name + ".wdt")
		);
		es.updateTable();
	}
	
	return null;
	}
	
	public FileFilter getFiter() {
		return new FileFilter() {

			public boolean accept(File arg0) {
				if(arg0.isDirectory())return true;
				if(arg0.toString().toLowerCase().endsWith(".wdt")) return true;
				else return false;
			}

			public String getDescription() {
				return "WOW WDT Files";
			}
		};
	}

	public wowfile load(File arg0) {
		wdt ret;
		
		try {
			ret = new wdt( fileLoader.openBuffer(arg0.getAbsolutePath()) );
		} catch(Exception e) {
			return null;
		}
		return ret;
	}

	public int save(openedFile file) {
		if(file.obj instanceof wowfile) {
			wowfile o = (wowfile)file.obj;
			o.render();
			fileLoader.saveBuffer(o.buff , file.f.getAbsolutePath());
			return 1;
		}
		return -1;
	}
}
