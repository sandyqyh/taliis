package de.taliis.plugins.dialogs;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import starlight.alien.*;
import starlight.taliis.apps.editors.AdvJTableCell;
import starlight.taliis.apps.editors.TableColorRenderer;
import starlight.taliis.core.files.*;
import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.wdt.MODF;
import starlight.taliis.core.chunks.wdt.MODF_Entry;
import starlight.taliis.core.chunks.wdt.MWMO;
import starlight.taliis.helpers.fileLoader;
import starlight.taliis.helpers.wdtObjHelper;

/**
 * JTable interface for edit MTEX chunks in ADT files
 * @author tharo
 *
 */

public class wdtObjectAppTable extends JPanel 
						implements ActionListener {
	wdt obj;
	JTable table;
	JToolBar toolBar;
	
	final String ADD = "add";
	final String DEL = "del";
	
	public wdtObjectAppTable(wdt reference) {
		obj = reference;
		this.setLayout(new BorderLayout());
			
		toolBar = new JToolBar();
		JButton  button = makeNavigationButton("delete", DEL,
                "Delete given texture File",
                "Remove Texture");
		toolBar.add(button);
		button = makeNavigationButton("add", ADD,
                "Add a new texture File",
                "Add Texture");
		toolBar.add(button);
		toolBar.addSeparator();
		add(toolBar, BorderLayout.PAGE_START);
			
        table = new JTable(new wdtmodfTableModel(obj.modf, obj.mwmo));
        //table.setPreferredScrollableViewportSize(new Dimension(70, 70));

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.addComponentListener(new
                CorrectStrangeBehaviourListener(table, scrollPane)); 
        
        // set some table look and feel
        initTableFace();
        
        //Add the scroll pane to this panel.
        add(scrollPane);
	}
	
	void initTableFace() {
		TableColumn column;
		
		column = table.getColumnModel().getColumn(0);
        column.setPreferredWidth(50);
        column.setCellRenderer(new TableColorRenderer(Color.WHITE));
        
        table.getColumnModel().getColumn(1).setMaxWidth(90);
		
		for(int c=2; c<5; c++) {
			column = table.getColumnModel().getColumn(c);
			column.setMaxWidth(150);
			column.setCellRenderer(new TableColorRenderer(new Color(224,255,224)));
		}
		for(int c=6; c<9; c++) {
			column = table.getColumnModel().getColumn(c);
			column.setMaxWidth(150);
			column.setCellRenderer(new TableColorRenderer(new Color(224,255,224)));
		}
	}

	protected JButton makeNavigationButton(String imageName,
            String actionCommand,
            String toolTipText,
            String altText) {
		//Look for the image.
		String imgLocation = "images/icons/"
		+ imageName
		+ ".png";

		//Create and initialize the button.
		JButton button = new JButton();
		button.setActionCommand(actionCommand);
		button.setToolTipText(toolTipText);
		button.addActionListener(this);
		
		if (imageName != "" && imageName != null) {    //image found
			button.setIcon(fileLoader.createImageIcon(imgLocation));
		} 
		else {                                     //no image found
			button.setText(altText);
			System.err.println("Resource not found: " + imgLocation);
		}

		return button;
	}

	
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        
        if (ADD.equals(cmd)) {
        	if(obj.mwmo.getLenght()==0) {
        		JOptionPane.showMessageDialog(this.getParent(),
        				"No Objects registered!",
        				"Error",
        				JOptionPane.ERROR_MESSAGE);
        		return;
        	}
        	wdtObjHelper objh = new wdtObjHelper(obj);
        	Random r = new Random();
        	objh.addWMO(0, r.nextInt(100000)+1, r.nextInt(40)+2, r.nextInt(30)+2, r.nextInt(40)+2);
        	
        	table.updateUI();
        }
        else if (DEL.equals(cmd)) {
        	if(table.getSelectedRow()==-1) return;
        	obj.modf.remove( table.getSelectedRow()  );
        	table.updateUI();
        }
    }

}



class wdtmodfTableModel extends AbstractTableModel {

	MODF archive;
	MWMO files;
	
	// Bezeichnungen
    Vector columnNames = new Vector();
    
    wdtmodfTableModel(MODF reference, MWMO mwmo) {
    	archive = reference;
    	files = mwmo;
    }
    
    public int getColumnCount() {
        return 11;
    }

    public int getRowCount() {
    	return (int)archive.getLenght();
    }

    public String getColumnName(int col) {
        switch(col) {
        	case 0: return "File ID";
        	case 1: return "Unique ID";
        	case 2: return "x";
        	case 3: return "y";
        	case 4: return "z";
        	case 5: return "flags";
        	case 6: return "rot a";
        	case 7: return "rot b";
        	case 8: return "rot c";
        	case 9: return "DD Set";
        	case 10: return "Name Set";
        }
        return "";
    }

    public Object getValueAt(int row, int col) {
    	switch(col) {
	    	case 0:  {
	    		int id = archive.entrys[row].getNameID();
	    		String filename = files.getValueNo(id);
	    		return new AdvJTableCell(id, filename);
	    	}
	    	case 1: return archive.entrys[row].getUniqID();
	    	case 2: return archive.entrys[row].getX();
	    	case 3: return archive.entrys[row].getY();
	    	case 4: return archive.entrys[row].getZ();
	    	case 5: return archive.entrys[row].getFlags();
	    	case 6: return archive.entrys[row].getA();
	    	case 7: return archive.entrys[row].getB();
	    	case 8: return archive.entrys[row].getC();
	    	case 9: return archive.entrys[row].getDoodadSet();
	    	case 10: return archive.entrys[row].getWMONameSet();
    	}
	    return null;
    }

/*    public Class getColumnClass(int c) {
        if(c==0) return Icon.class;
        else return String.class;
    }/**/

    public boolean isCellEditable(int row, int col) {
    //	if(col==1) return true;
    	return true;
    }

    public void setValueAt(Object value, int row, int col) {
    	// translate value
    	int vInt = 0;
    	float vFloat = 0;
    	short vShort = 0;
	    try {
	    	switch(col) {
	    		case 0: case 1: case 5:
	    			// integer value
	    			vInt = Integer.valueOf(value.toString());
	    			break;
	    		case 9: case 10:
	    			vShort = Short.valueOf(value.toString());
	    			break;
	    		case 2: case 3: case 4: case 6: case 7: case 8:
	    			// float value
	    			vFloat = Float.valueOf(value.toString());
	    			break;
	    		default:
	    			return;
	    	}
	    } catch(Exception exp) {
	    	return;
	    }
	// unique id	TODO: check for aviability
	    switch(col) {
	    	case 0:
	    		if(vInt>=0 || vInt<archive.getLenght())
	    	    		archive.entrys[row].setNameID(vInt);
	    		break;
	    	case 1: archive.entrys[row].setUniqID(vInt); break;
	    	case 2: archive.entrys[row].setX(  vFloat  ); break;
	    	case 3: archive.entrys[row].setY(  vFloat  ); break;
	    	case 4: archive.entrys[row].setZ( vFloat ); break;
	    	case 5: archive.entrys[row].setFlags(vInt); break;
		    case 6: archive.entrys[row].setA(vFloat); break;
	    	case 7: archive.entrys[row].setB(vFloat); break;
	    	case 8: archive.entrys[row].setC(vFloat); break;
	    	case 9: archive.entrys[row].setDoodadSet(vShort); break;
	    	case 10: archive.entrys[row].setWMONameSet(vShort); break;
	    }
    }
    
    public void clear() {
    }
}
