package de.taliis.editor.plugin;

import javax.swing.JPanel;

/**
 * Interface for all editors / views
 * 
 * @author ganku
 */


public interface PluginView {
	/**
	 * Creates the "editor/view" and gives the reference back
	 * @return JPanel reference to the view
	 */
	public abstract JPanel createView();
}
