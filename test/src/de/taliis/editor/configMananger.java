package de.taliis.editor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;
import java.util.Vector;

/**
 * The config Mananger is written to allow the following
 * actions:
 * 
 * 1.	Load / Store the global config File
 *  .2	if not exist create a absolute minimum config file
 * 2.	Request an aditional config file to be loaded (read only)
 *  .2	also alow that from a different class loader (for plugins)
 * 
 * All actions have to be done in the basic file system.
 * 
 * @author Tharo Herberg
 */
public class configMananger {
	private Properties globalConfig;
	private String path = "main.conf";
	private Vector<openedFile> files;
	
	
	public configMananger() {
		files = new Vector<openedFile>();
		loadGlobalConfig();
	}
	
	/**
	 * Request a given config file 
	 * @param path
	 * @return
	 */
	public Properties requestConfig(String path) {
		return null;
	}
	
	/**
	 * load std config file .. or trys to do so.
	 */
	private void loadGlobalConfig() {
    	InputStream s = null;
    	globalConfig = new Properties();
    	
    	try {
    		File f = new File(path);
			s = new FileInputStream(f);		
    	} catch (FileNotFoundException e) {
			//e.printStackTrace();
		} 
    	
    	if(s==null)
    		s = ClassLoader.getSystemResourceAsStream(path);
    	
    	if(s==null) return;
    	try {
    		globalConfig.load(s);
			s.close();
    	}
    	catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * write std config file. or trys to do so
	 */
	private void storeGlobalConfig() {
		OutputStream o = null;
		
		try {
    		File f = new File(path);
    		o = new FileOutputStream(path);

    		globalConfig.store(o,
    				"Taliis WoW Editor\n" + 
    				"www.taliis.de | taliis.blogspot.com \n" +
    				""
    			);
    		o.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	
	/**
	 * Writes all config files back to its source
	 */
	public void unload() {
		//TODO close handles
		
		// store config
		storeGlobalConfig();
	}
	
	/**
	 * @return global config File
	 */
	public Properties getGlobalConfig() {
		return globalConfig;
	}
	
}
