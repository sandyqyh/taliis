package starlight.taliis.helpers;

import java.awt.image.BufferedImage;
import starlight.taliis.core.files.*;
import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.adt.MCNK;
import starlight.taliis.core.chunks.adt.MCVT;

/**
 * This helper provides functions to set the xyz data property,
 * calculate normals and also to read in a height map from a picture
 * resource
 *  
 * @author tharo
 *
 */

public class adtCoordHelper {
	adt obj;
	
	public adtCoordHelper(adt mainObject) {
		obj = mainObject;
	}
	
	/**
	 * Calculates and sets all x and y data for the MCNK chunks
	 * by the given wdt coordinates. z is a default value.
	 * Because its needed to get displayed it also adds one layer
	 * and set its texture to the default texture
	 * 
	 * @param wdtX
	 * @param wdtY
	 * @param z
	 * @param texture Default texture ID for the first layer
	 */
	public void calcCoordinates(int wdtX, int wdtY, float z, int texture) {
		float xbase, ybase;
		float adtWidth = 16*MCNK.gfxWidth;
		
		// calculate start position		
		xbase = (32-wdtX) * adtWidth;
		ybase = (32-wdtY) * adtWidth;
		
		// set x/y positions
		for(int x=0; x<16; x++) {
			for(int y=0; y<16; y++) {
				int tile = x*16 + y;
				
				obj.mcnk[tile].setPosX(xbase - x*MCNK.gfxWidth);
				obj.mcnk[tile].setPosY(ybase - y*MCNK.gfxWidth);
				obj.mcnk[tile].setPosZ(z);
		
				// add texture + layer
				obj.mcnk[tile].createLayer(texture, 32);
			}
		}
	}

	/**
	 * Just move the coodinates of the field to a new
	 * global ADT location.
	 * 
	 * TODO: Its also needed to reset the Obj and DD 
	 * coordinates!
	 * 
	 * @param wdtX
	 * @param wdtY
	 */
	public void moveCoordinates(int wdtX, int wdtY) {
		float xbase, ybase;
		float adtWidth = 16*MCNK.gfxWidth;
		
		// calculate start position		
		xbase = (32-wdtX) * adtWidth;
		ybase = (32-wdtY) * adtWidth;
		
		// set x/y positions
		for(int x=0; x<16; x++) {
			for(int y=0; y<16; y++) {
				int tile = x*16 + y;
				
				obj.mcnk[tile].setPosX(xbase - x*MCNK.gfxWidth);
				obj.mcnk[tile].setPosY(ybase - y*MCNK.gfxWidth);
			}
		}
	}
	
	
	/**
	 * Loads a with filename specified height map picture
	 * and add its blue values into our fields.
	 * 
	 * Note that only the blue value counts.
	 * Note that values from 0-55 are below map 0 and 56-255 above.
	 * Note that pictures resolution have to be 136x136 pixels.
	 * Note that LOD get calculated
	 * 
	 * calcLOD() get called for each of the fields.
	 * 
	 * @param FileName 
	 * @param multiplier Value by that 1 of the height map get multiplied with
	 */
	public void loadHeightMap(String FileName, float multiplier) {
		int zero = 55;
		int dimension = 136;
		
		BufferedImage grey = fileLoader.loadImage(FileName);
		
		// no to small maps please
		if(grey.getWidth()<dimension || grey.getHeight()<dimension) return;
		
		// catch values
		int[][] values = new int[dimension][dimension];
		for(int y=0; y<dimension; y++) {
			grey.getRGB( 0, y, dimension, 1, values[y], 0, dimension);
		}
		grey = null;
		
		
		
		// calculate and place height values
		for(int fieldY=0; fieldY<16; fieldY++) {
			for(int fieldX=0; fieldX<16; fieldX++) {
				// add 8x8 of the pixels
				for(int y=0; y<9; y++) {
					int picY = fieldY*8+y;
					
					for(int x=0; x<9; x++) {
						int picX = fieldX*8+x;
						float tmp = ( values[picX][picY] & 0xff) - zero;
						
						// set value
						obj.mcnk[fieldY*16 + fieldX].mcvt.setValNoLOD(x, y, tmp * multiplier);
					}
				}
				
			}
		}
		
		// calculate LOD values
		for(int c=0; c<256; c++)
			calcLOD(c);
	}
	
	/**
	 * This functions calculates the LOD values by its surrounding
	 * "default" height values.
	 * 
	 * @param MCNKIndex
	 */
	public void calcLOD(int MCNKIndex) {
		MCVT o = obj.mcnk[MCNKIndex].mcvt;
		
		for(int y=0; y<8; y++) {
			for(int x=0; x<8; x++) {
				float a, b, c, d, r;
				a = o.getValNoLOD(x, y);
				b = o.getValNoLOD(x+1, y);
				c = o.getValNoLOD(x, y+1);
				d = o.getValNoLOD(x+1, y+1);
				
				r = (a+b+c+d) / 4; 
				
				o.setValLOD(x, y, r);
			}
		}
	}
}
