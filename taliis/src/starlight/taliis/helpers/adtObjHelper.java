package starlight.taliis.helpers;

/**
 * This class provides functions that allows to 
 * create / modify / delete DD and WMO Objects inside of a map.
 * It do NOT handle the registration/management of the Filenames itself.
 * 
 * @author tharo
 *
 */

import starlight.taliis.core.files.*;
import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.adt.MCNK;
import starlight.taliis.core.chunks.adt.MCRF;
import starlight.taliis.core.chunks.adt.MDDF_Entry;
import starlight.taliis.core.chunks.adt.MODF_Entry;

public class adtObjHelper {
	adt obj;
	
	public adtObjHelper(adt mainObject) {
		obj = mainObject;
	}

	
	public int addWMO(int WMOFileID, int globalID) {
		// create appeareance
		int modfid = obj.modf.addObject(WMOFileID, globalID, 
				MODF_Entry.translate( obj.mcnk[0].getPosX()), 
				MODF_Entry.translate( obj.mcnk[0].getPosY()), 
				obj.mcnk[0].getPosZ()
			);
		
		// calculate fake ext values
		obj.modf.entrys[modfid].calcExtValues();
		
		return modfid;
	}
	
	/**
	 * Adds a new WMO or such-called "object" at the given
	 * position  and register it in the MCNK chunk.
	 * 
	 * @param WMOFileID	ID were the wmo file got loaded with
	 * @param globalID	Global and unique ID in this instance
	 * @param x			x coordinates (absolute)
	 * @param y			y coordinates (absolute)
	 * @param z			y coordinates (absolute)
	 * @return			index of the entry in MODF list
	 */
	public int addWMO(int WMOFileID, int globalID,
			float x, float y, float z) {
		
		// create appeareance
		int modfid = obj.modf.addObject(WMOFileID, globalID, 
				MODF_Entry.translate(x), 
				MODF_Entry.translate(y), 
				z
			);
		
		// calculate fake ext values
		obj.modf.entrys[modfid].calcExtValues();

		// witch is our MCNK tile?
		MCNK field = getLocationObj(x, y);
		
		// dont forget to register it
		field.mcrf.appendEntry( modfid );
		
		// increase total numbers of objects there
		int count = field.getNObj();
		field.setNObj(count+1);
		
		return modfid;
	}
	
	/**
	 * A very lazy and almost dirty way to create an new
	 * Doodad entry. It places the DD by default to the first
	 * point the map given by the MCNK chunk.
	 * 
	 * The app will not get registered at this MCNK field!
	 * 
	 * @param DDFileID
	 * @param globalID
	 * @return
	 */
	public int addDoodad(int DDFileID, int globalID) {
		// create appeareance
		int mddfid = obj.mddf.addDoodad(DDFileID, globalID, 
				MODF_Entry.translate( obj.mcnk[0].getPosX()), 
				MODF_Entry.translate( obj.mcnk[0].getPosY()), 
				obj.mcnk[0].getPosZ()
			);
		return mddfid;
	}
	/**
	 * Adds a new doodad at the given
	 * position and register it in the MCNK chunk.
	 * 
	 * @param DDFileID	ID were the wmo file got loaded with
	 * @param globalID	Global and unique ID in this instance
	 * @param x			x coordinates (absolute)
	 * @param y			y coordinates (absolute)
	 * @param z			y coordinates (absolute)
	 * @return			index of the entry in MDDF list
	 */
	public int addDoodad(int DDFileID, int globalID,
			float x, float y, float z) {
		
		// create appeareance
		int mddfid = obj.mddf.addDoodad(DDFileID, globalID, 
				MODF_Entry.translate(x), 
				MODF_Entry.translate(y), 
				z
			);
		
		
		// witch is our MCNK tile?
		MCNK field = getLocationObj(x, y);
		
		// dont forget to register it
		field.mcrf.insertEntry( mddfid );
		
		// increase total numbers of objects there
		int count = field.getNDoodad();
		field.setNDoodad(count+1);

		return mddfid;
	}
	
	/**
	 * Registers all MDDF and MODF entrys in theyr
	 * given MCNK.MCRF sub-chunk.
	 * 
	 * All given MCRF chunks get deleted an regenerated so
	 * u can use this as a "clean" rebuild.
	 *
	 * It does not handle unsharpen. 
	 * (multible registrations of bordered objects)
	 *
	 */
	public void generateAppeareances() {
		// clear all chunks
		for(int c=0; c<256; c++) {
			obj.mcnk[c].mcrf = new MCRF();
			obj.mcnk[c].setNDoodad(0);
			obj.mcnk[c].setNObj(0);
		}
		
		// Doodads
		for(int c=0; c<obj.mddf.getLenght(); c++) {
			// witch is our MCNK tile?
			MDDF_Entry dd = obj.mddf.entrys[c];
			MCNK field = getLocationObj(
					MODF_Entry.translate(dd.getX()), 
					MODF_Entry.translate(dd.getY())
				);
			
			if(field==null) {
				System.err.println("DD " + c + " is located too far from this map. Skipped.");
			}
			else {
				// dont forget to register it
				field.mcrf.appendEntry(c);
				
				// increase total numbers of objects there
				int count = field.getNDoodad();
				field.setNDoodad(count+1);
			}
		}
		
		// WMO
		for(int c=0; c<obj.modf.getLenght(); c++) {
			// witch is our MCNK tile?
			MODF_Entry ob = obj.modf.entrys[c];
			MCNK field = getLocationObj(
					MODF_Entry.translate(ob.getX()), 
					MODF_Entry.translate(ob.getY())
				);
			
			if(field==null) {
				System.err.println("Object " + c + " is located too far from this map. Skipped.");
			}
			else {
				// dont forget to register it
				field.mcrf.appendEntry(c);
				
				// increase total numbers of objects there
				int count = field.getNObj();
				field.setNObj(count+1);
			}
		}
	}
	
	/**
	 * Returns the x/y tile location of the field were this
	 * coordinates are located. Returns NULL if its outside of
	 * this ADT map. 
	 * Tile locations start at 0! Dont forget that!
	 * 
	 * @param x	position we want to know
	 * @param y position we want to know
	 * @return [0] = x, [1] = y index of the field. NULL if outside of this ADT.
	 */
	public int[] getLocation(float findX, float findY) {
		int[] retu = {-1, -1};
		float xbase, ybase;
		// to prevend rounding failures or just realsize "save doubles"
		final float UNSHARPEN = 0.2f;
		
		// calculate start position		
		xbase = obj.mcnk[0].getPosX() + UNSHARPEN;
		ybase = obj.mcnk[0].getPosY() + UNSHARPEN;
		
		//System.out.println("Base: " + xbase + "/" + ybase);
		
		// set x/y positions
		for(int x=0; x<16; x++) {
			for(int y=0; y<16; y++) {
				if(findX<xbase && findX>(xbase-MCNK.gfxWidth-UNSHARPEN))
					retu[0] = x;
				if(findY<ybase && findY>(ybase-MCNK.gfxWidth-UNSHARPEN))
					retu[1] = y;
			
				if(retu[0]!=-1 && retu[1]!=-1)
					return retu;
			
				ybase-=MCNK.gfxWidth;
			}
			xbase-=MCNK.gfxWidth;
		}
		return null;
		/**/
		/*
		float sx = obj.mcnk[0].getPosX();
		float sy = obj.mcnk[0].getPosY();

		// what modifiers do we have between the fields?
		float xmod = obj.mcnk[16].getPosX()-sx;
		float ymod = obj.mcnk[1].getPosY()-sy;
		
		for(int c=0; c<16; c++) {
			float v = sx+xmod;
			if(findX<=sx && findX>=v ||
				findX>=sx && findX<=v) {
				retu[0]=c;
				break;
			}
			//System.out.println(sx + "  -  " + x + "  -  " + v);
			sx=v;
		}
		
		for(int c=0; c<16; c++) {
			float v = sy+ymod;
			if(findY<=sy && findY>=v ||
				findY>=sy && findY<=v) {
				retu[1]=c;
				break;
			}
			sy=v;
		}

		if(retu[0]==-1 || retu[1]==-1) return null;
		
		else return retu;/**/
	}
	
	/**
	 * lazy wrap around for getLocation that directly returns
	 * the corospodending MCNK object
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public MCNK getLocationObj(float x, float y) {
		int[] tmp = getLocation(x, y);
		if(tmp==null) return null;
		else return obj.mcnk[16*tmp[0] + tmp[1]];
	}
}
