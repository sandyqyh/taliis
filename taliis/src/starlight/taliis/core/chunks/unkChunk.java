package starlight.taliis.core.chunks;

import java.nio.ByteBuffer;

/**
 * The "unknown chunk" class is created to have a standard class that does
 * nothing but reading the size field and reseting the buffer's position. The
 * chunk's name <i>might</i> be ignored.
 * 
 * @author ganku
 * @see chunk
 */
public class unkChunk extends chunk {
	/**
	 * Fake initialisation including a chunk-name-check.
	 * 
	 * @param pointer The usual buffer.
	 * @param magic The <b>magic</b>, not the chunk name.
	 * 
	 * @throws ChunkNotFoundException In case that the unknown chunk is not even
	 *             found.
	 */
	public unkChunk(ByteBuffer pointer, String magic) throws ChunkNotFoundException {
		super(pointer, magic);

		buff.limit(data + buff.getInt(chunkSize));
		pointer.position(pointer.position() + buff.limit());
	}
	
	public unkChunk(String magic) throws ChunkNotFoundException {
		super(data);

		buff.put(magic.getBytes());
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}

	/**
	 * Read that unknown chunk from a pointer.
	 * @param pointer The ByteBuffer containing the chunk.
	 */
	public unkChunk(ByteBuffer pointer) {
		super(pointer);
		
		buff.limit(data + buff.getInt(chunkSize));
		pointer.position(pointer.position() + buff.limit());
	}
}
