package starlight.taliis.core.chunks.adt;

/**
 * liquit map
 * includes data about the liquit state of this field ..
 * SIZE field is always zero (dont know why)
 * 
 * http://wowdev.org/wiki/index.php/ADT#MCLQ_sub-chunk
 * 
 * @author conny
 *
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MCLQ extends chunk {
	int size;
	public boolean isempty=false;
	public MCLQ_HEntry height[];
	public Flags_Entry flags[];
	public byte[] unk;
	public float altitude=0;
	public float baseheight=0;
	/*
	 * 0x0	magic
	 * 0x4	size
	 * 0x8	data
	 * 		1*float=water altitude
	 * 		1*float=base height
	 * 		9*9 vertex heightmap
	 * 		8*8 filler flags		value 0x0F means do not render. 
	 * 		...it's more likely that b01 and b10 and b11 mean do not render..whatever
	 * 		0x54 bytes of unknown stuff ..
	 */
	public final static int unknownDataSize = 0x54;
	
	MCLQ(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "QLCM");

		//NOTE: Size field is always zero!
		size = buff.getInt();
		
		// are we empty?
		if(chunk.nextChunk(buff).compareTo("ESCM")==0||chunk.nextChunk(buff).compareTo("KNCM")==0) {
			size = 0;
			isempty=true;
		}
		else {
			altitude=buff.getFloat();
			baseheight=buff.getFloat();
			height = new MCLQ_HEntry[9*9]; 
			for(int c=0; c<9*9; c++)
				height[c] = new MCLQ_HEntry(buff);
			
			flags = new Flags_Entry[8*8];
			for(int k=0;k<8*8;k++){
				flags[k] = new Flags_Entry(buff);
			}
			
			//TODO: do not ignore empty area and fillers
			//TODO: find out WHY the hell 0 other bytes!
			//0x54 has to do something with appearing of water?
			unk=new byte[0x54];
			for(int i=0;i<0x54;i++)
				unk[i]=buff.get();
			buff.position( buff.position());
			
			// empty stuff on our end?
			/*if(chunk.nextChunk(buff).compareTo("ESCM")!=0) {
				buff.position( buff.position() );
				if(chunk.nextChunk(buff).compareTo("ESCM")!=0) {
					System.err.println("PANIC! No liquid size seems to fit!");
				}
			}	*/
		}
		
		buff.limit(buff.position());
		pointer.position( pointer.position() + buff.limit());
	}
	
	public MCLQ() {
		super(data+8+8*8+9*9*8+0x54);
		byte magic[] = { 'Q', 'L', 'C', 'M'};
			
		buff.put(magic);	// magic
		buff.putInt(0);		// size as it's always null...
		buff.putFloat(0);
		buff.putFloat(0);
		height = new MCLQ_HEntry[9*9];
		for(int i=0;i<9*9;i++)
			height[i]=new MCLQ_HEntry();
		flags = new Flags_Entry[8*8];
		for(int i=0;i<8*8;i++){
			flags[i]=new Flags_Entry();
			//I could set this also to null but... it's more fun setting this to 0x4^^
			flags[i].setFlag((byte) 0x4);
		}
		unk=new byte[0x54];
		for(int i=0;i<unknownDataSize;i++)
			unk[i]=0;
		// reset position
		buff.position(0);
		
		if(DEBUG) System.out.println("  MCLQ created.");
	}
	
	/**
	 * Render our liquit datas
	 */
	public void render() {
		if(isempty)
		buff.position(0);
		else{
			ByteBuffer tmp = doRebirth(data+8+8*8+9*9*8+0x54);
			// add magic
			tmp.position(magic);
			byte magic[] = { 'Q', 'L', 'C', 'M'};
			tmp.put(magic);
			// copy all data
			tmp.position(data);
			tmp.putFloat(altitude);
			tmp.putFloat(baseheight);
			for(int i=0;i<9*9;i++)
				tmp.put(height[i].buff);
			for(int i=0;i<8*8;i++)
				tmp.put(flags[i].buff);
			for(int i=0;i<unknownDataSize;i++)
				tmp.put(unk[i]);
			//size is always null so set no size^^
			buff=tmp;
			buff.position(0);
		}
	}
	
	public void setHeight(int pos,float val){
		height[pos].setHeight(val);
	}
	public int getHeight(int pos){
		return height[pos].getHeight();
	}
}

/**
 * Info about the _height_ of water i think
 * @author conny
 *
 */
class MCLQ_HEntry extends chunk {
	/*0x00*/ 	public final static int flag = 0x0;
	/*0x02*/	public final static int unk2 = 0x2;
	/*0x04*/	public final static int value = 0x4;
										//float	height value
	/*0x08*/	public final static int end = 0x8;
	
	MCLQ_HEntry(ByteBuffer pointer) {
		super(pointer);
		
		buff.limit(end);
		pointer.position(pointer.position() + buff.limit());
	}
	
	MCLQ_HEntry(){
		super(end);
		buff.position(0);
	}
	
	public void setHeight(float val){
		buff.putFloat(value, val);
	}
	public int getHeight(){
		return buff.getInt(value);
	}
}
	
class Flags_Entry extends chunk{
	Flags_Entry(ByteBuffer pointer){
		super(pointer);
		
		buff.limit(1);
		pointer.position(pointer.position() + buff.limit());
	}
	Flags_Entry(){
		super(1);
		buff.position(0);
	}
	public void setFlag(byte val){
		buff.put(0,val);
	}
	public byte getFlag(){
		return buff.get(0);
	}
}
