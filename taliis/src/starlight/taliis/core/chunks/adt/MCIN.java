package starlight.taliis.core.chunks.adt;

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * MCIN chunk - Info chunk about all 256 MCNK chunks
 * @author conny
 *
 */

public final class MCIN extends chunk {
	public MCIN_Entry entrys[];
	int size;
	
	/*
	 * 0x0	magic
	 * 0x4	size
	 * 0x8	data
	 */
	
	public MCIN(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "NICM");
		
		size = buff.getInt();
		
		// init entrys
		entrys = new MCIN_Entry[256];
		for(int c=0; c<256; c++)
			entrys[c] = new MCIN_Entry(buff);
		
		// size + magic
		buff.limit(size + 8);
		
		// push pointers
		pointer.position( pointer.position() + buff.limit());
	}
	
	
	public MCIN() {
		super(8 + 256 * MCIN_Entry.end);
		byte magic[] = { 'N', 'I', 'C', 'M'};
		
		buff.put(magic);					// put magic
		buff.putInt(256 * MCIN_Entry.end);	// put size
		
		// init objects
		entrys = new MCIN_Entry[256];
		for(int c=0; c<256; c++)
			entrys[c] = new MCIN_Entry(buff);
		
		// reset position
		buff.position(0);	
		
		if(DEBUG) System.out.println("Empty MCIN created.");
	}
	
	/**
	 * Set a offset :) And then go home! >:|
	 * @param index
	 * @param value
	 */
	public void setOffs(int index, int offs, int size) {
		entrys[index].setOffset(offs);
		entrys[index].setSize(size);
	}
	public int getOffs(int index) {
		return entrys[index].getOffset();
	}
}