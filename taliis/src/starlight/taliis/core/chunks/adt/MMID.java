package starlight.taliis.core.chunks.adt;


/**
 * MMID Chunk 
 * Lists the relative offsets of string beginnings in the above MMDX chunk. (sort of redundant) One 32-bit integer per offset.
 * 
 * @author conny
 *
 */

import java.nio.*;

import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;


public final class MMID extends chunk {
	int size = 0;
	
	/*
	 * 0x0 magic
	 * 0x4 size
	 * 0x8 data
	 */
	
	public MMID(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "DIMM");
		
		size = buff.getInt();
		
		buff.limit( size + data );
		pointer.position( pointer.position() + buff.limit() );
	}
	
	public MMID() {
		super(0x8);
		byte magic[] = { 'D', 'I', 'M', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);	
		
		if(DEBUG) System.out.println("Empty MMID created.");
	}
	
	/**
	 * catch all offsets and write them down
	 * @param data
	 */
	public void render(MMDX pointer) {
		super.render();
		if(pointer.getLenght()==0) return;
		int ns = pointer.getLenght()*4;

		// extend our object
		buff = this.doRebirth(ns + data);
		writeSize();
		
		// catch up all offsets
		buff.putInt(0);
		
		// look into all memory except header
		int max = pointer.getSize() - data;
		byte mem[] = new byte[max];
		pointer.buff.position(data);
		pointer.buff.get(mem);
		
		for(int c=1; c<max; c++) {
			// +1 coz we want offset _after_ NULL
			if(mem[c-1]==0) buff.putInt(c);
		}
		
		buff.position(0);
		pointer.buff.position(0);
	}

	/**
	 * returns the offset of entry index
	 * @param index
	 * @return
	 */
	public int getOffsetNo(int index) {
		buff.position(data + index*4);
		return buff.getInt();
	}
	
	public int getLenght() {
		return (getSize()-data)/4;
	}
}
