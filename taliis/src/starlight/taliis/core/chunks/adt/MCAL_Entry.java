package starlight.taliis.core.chunks.adt;

import java.nio.ByteBuffer;
import starlight.taliis.core.memory;

/**
 * Alpha map layer entry
 * For every layer, a 32x64 array of alpha values.
 * that means that each field needs 4 bit.
 *
 * @author conny
 *
 */
public final class MCAL_Entry extends memory {
	public final static int end = 32*64;	// each entry = 4 bit
	ByteBuffer buffi;
	
	public MCAL_Entry(ByteBuffer pointer) {
		super(pointer);
		buff.limit(end);
		buffi=buff;
		pointer.position( pointer.position() + buff.limit() );
	}

	/**
	 * Creates a new and empty entry
	 */
	public MCAL_Entry() {
		super(end);
	}
	
	public ByteBuffer getPixels(){
		return buff;
	}

	
	/**
	 * Get given alpha Value (allready shifted <<4)
	 * @param n position (0-63)
	 * @return
	 */
	public int getValue(int n) {
		int t = buff.get(n/2);
		if(n%2==0) return ((t & 0xF)<<4);
		else return (t & 0xF0);
	}

	
	/**
	 * Set the given alpha value.
	 * Value will be shifted >>4!
	 * 
	 * @param n position (0-63)
 	 * @param value the value
	 */
	public void setValue(int n, int value) {
		int t = buff.get(n/2);
		
		if(n%2==0) {
			t &= 0xF0;
			t |= (value>>4) & 0xF;
		}
		else {
			t &= 0xF;
			t |= value & 0xF0;
		}
		
		buff.put(n/2, (byte)t);
	}
}
