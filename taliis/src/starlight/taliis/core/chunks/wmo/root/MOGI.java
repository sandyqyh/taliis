package starlight.taliis.core.chunks.wmo.root;

import java.nio.ByteBuffer;

import starlight.taliis.core.ZeroPaddedString;
import starlight.taliis.core.chunks.ChunkNotFoundException;
import starlight.taliis.core.chunks.chunk;

/**
 * Well. Infos about the DD sets.
 * http://www.madx.dk/wowdev/wiki/index.php?title=WMO#MOGI_chunk
 * 
 * @author ganku
 *
 */

public class MOGI extends chunk {
	MOGI_Entry entrys[] = null;
	
	public MOGI(ByteBuffer pointer) throws ChunkNotFoundException {
		super(pointer, "IGOM");

		int size = buff.getInt();
		buff.limit(buff.position() + size);
		
		int l = size / MOGI_Entry.end;
		entrys = new MOGI_Entry[l];
		
		for(int c=0; c<l; c++)
			entrys[c] = new MOGI_Entry(buff);
		
		// push
		pointer.position( pointer.position() + buff.limit());
	}

	public MOGI() {
		super(data);
		byte magic[] = { 'I', 'G', 'O', 'M'};
		
		buff.put(magic);	// magic
		buff.putInt(0);		// size
		
		// reset position
		buff.position(0);
	}

	/**
	 * Get the entry no n
	 * @param n
	 * @return MOGI_Entry item
	 */
	public MOGI_Entry getEntry(int n) {
		if(n<0 || n>=entrys.length) return null;
		else return entrys[n];
	}
	
	public int getLenght() {
		if(entrys==null) return -1;
		else return entrys.length;
	}
}
