package starlight.taliis.core.chunks;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import starlight.taliis.core.memory;

/**
 * Basis Class for all data chunks "chunk" Provides a set of constants that are
 * equal in each WoW Data Chunk.
 * 
 * It also provides a set of methods to detect correct habits and makes working
 * with chunks just more easy.
 * 
 * @author tharo
 */
public class chunk extends memory {
	// Offsets that are the same in EVERY chunk
	/* 0x00 */public final static int magic = 0;
	/* 0x04 */public final static int chunkSize = 0x4;
	/* 0x08 */public final static int data = 0x8;

	protected String key;
	protected boolean changed = false;

	/**
	 * Init the data buffer by get a buffer that is allready set to the correct
	 * position.
	 * 
	 * This function DOES NOT set the pointer to the end by use the chunk size.
	 * This should done after reading in the data because WoW's size fields may
	 * be wrong sometimes.
	 * 
	 * @param pointer ByteBuffer with the opened File
	 * @param chunk_key The magic key to be checked.
	 * @throws ChunkNotFoundException Thrown if the chunk is not found.
	 */
	public chunk(ByteBuffer pointer, String chunk_key) throws ChunkNotFoundException {
		super(pointer);

		key = chunk_key;

		if (DEBUG == true)
			System.out.println("Looking for " + key + " at " + getHexAdr(pointer));

		// check our chunk id!
		if (checkChunkID() == false) 
			throw new ChunkNotFoundException("Expected: '" + key + "' but found '" + getRealChunkID() + "' "
					+ getHexAdr(pointer) + " (" + pointer.position() + ")", key);
		else
			if (DEBUG == true)
				System.out.println("\t.. Found it!");
	}

	/**
	 * Constructor for just slice the buffer without any magic word correction.
	 * 
	 * Note: If you want to copy it, call render() before it!
	 * 
	 * @param pointer A buffer where our chunk is stored.
	 */
	public chunk(ByteBuffer pointer) {
		super(pointer);

		key = "";
	}

	/**
	 * Constructor for creating chunks that need a buffer.
	 * 
	 * @param size The size of new chunk.
	 */
	public chunk(int size) {
		super(size);
	}
	
	/**
	 * The copy constructor for chunks. 
	 * Note: It calls the sources render() function!
	 *  
	 * @param source
	 */
	public chunk(chunk source) {
		source.render();
		
		ByteBuffer tmp = ByteBuffer.allocate(source.buff.limit());
		tmp.order(ByteOrder.LITTLE_ENDIAN);
		
		tmp.position(0);
		source.buff.position(0);
		
		tmp.put(source.buff);
		
		tmp.position(0);
		source.buff.position(0);
		
		this.buff = tmp;
		this.render();
	}


	
	/**
	 * Not implemented! Use limit() you lasy bastard!
	 * 
	 * @param new_size The new size of the chunk.
	 */
	// TODO: Implement it.
	protected void smoot(int new_size) {
	}

	/**
	 * Checks if the magic key is the right one.
	 * 
	 * @return True if chunk is valid, false if not.
	 */
	public boolean checkChunkID() {
		return getRealChunkID().compareTo(key) == 0;
	}

	/**
	 * Get the chunk's identifier.
	 * 
	 * @return String with the correct, not reversed chunk ID.
	 */
	public String getRealChunkID() {
		byte mag[] = new byte[4];
		buff.position(0);
		buff.get(mag, 0, 4);

		return new String(mag);
	}

	/**
	 * Returns the real size of the ByteBuffer we use. This should always be
	 * chunksize + data-constant.
	 * 
	 * @return The size.
	 */
	public int getSize() {
		return buff.limit();
	}

	/**
	 * Recalculates the chunk's size and writes it into our data Buffer.
	 */
	public void writeSize() {
		// size offset 0x4, size without header
		buff.putInt(chunkSize, buff.limit() - data);
		// ? buff.position(0);
	}

	/**
	 * Static function that returns the next buffer's magic.
	 * 
	 * @param pointer The pointer of the first chunk.
	 * @return The identifier of the next one.
	 */

	// TODO: Outsource this?
	public static String nextChunk(ByteBuffer pointer) {
		ByteBuffer tmp = pointer.slice();

		byte mag[] = new byte[4];
		tmp.position(0);
		tmp.get(mag, 0, 4);

		return new String(mag);
	}

	/**
	 * Get the adress of a chunk as hex-String.
	 * 
	 * @param b The chunk.
	 * @return "0x{position}"
	 */
	public static String getHexAdr(ByteBuffer b) {
		int pos = b.position();
		return "0x" + Long.toHexString(pos);
	}

	/**
	 * Final call for all chunks. Renders the information together. This
	 * implementation prevents chunks to get written into bytebuffer without a
	 * real render(). If its not overwritten it just sets the buffer postion to
	 * 0.
	 */
	@Override
	public void render() {
		buff.position(0);
	}

	/**
	 * Creates a new ByteBuffer of size and copies 0x8 bytes of header values
	 * into it.
	 * 
	 * @param size Absolute size of the new chunk.
	 * @return The memory-buffer position after the header.
	 */
	@Override
	protected ByteBuffer doRebirth(int size) {
		ByteBuffer tmp = ByteBuffer.allocate(size);
		tmp.order(ByteOrder.LITTLE_ENDIAN);

		// copy first 8 bytes
		for (int c = 0; c < 8; c++)
			tmp.put(buff.get(c));

		return tmp;
	}

	/**
	 * Got my content changed? (bigger/smaller buffer size)
	 * 
	 * @return The flag changed. Does not actually check for changes!
	 */
	protected boolean isChanged() {
		return changed;
	}

	/**
	 * Set the "changed" flag.
	 */
	protected void Change() {
		changed = true;
	}

	/**
	 * Super method to get the number of subentries if available.
	 * 
	 * @return Always -1
	 * @deprecated Check spelling please! Use the right version now.
	 */
	@Deprecated
	public int getLenght() {
		return getLength();
	}

	/**
	 * Super method to get the number of subentries if available.
	 * 
	 * @return Always -1
	 */
	public int getLength() {
		return -1;
	}
}
