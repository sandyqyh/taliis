package starlight.taliis.core.binary.m2;

/**
 * With Wotlk an AnimationBlock points to a
 * substructure. These substructure is defined
 * here ^^
 * This points to an structure which may contain up
 * to 16byte
 * 
 * @author Tigurius
 */

import java.nio.ByteBuffer;

import starlight.taliis.core.memory;

public class AnimSubStructure extends memory{
	public static int nValues = 0x00;
	public static int ofsValues = 0x04;
	public static int end = 0x08;
	
	public AnimSubStructure(ByteBuffer databuffer){
		super(databuffer);
		buff.limit(end);
		databuffer.position( databuffer.position() + buff.limit());
	}
	public AnimSubStructure(){
		super(end);
	}
	
	public void render(){
		buff.position(0);
	}
	
	public int getnValues(){
		return buff.getInt(nValues);
	}
	public int getofsValues(){
		return buff.getInt(ofsValues);
	}
	public void setnValues(int val){
		buff.putInt(nValues,val);
	}
	public void setofsValues(int val){
		buff.putInt(ofsValues,val);
	}
}