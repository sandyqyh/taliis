package starlight.taliis.core.binary.m2.skeleton;

/**
 * List of animations present in the model. 
 * 
 * @author tigurius
 */

import java.nio.*;

import starlight.taliis.core.memory;


public class AnimSequ extends memory{
	public static int AnimationID =0x0;
	public static int SubAnimationID=0x2;
	public static int Length=0x4;
	public static int MovingSpeed=0x8;
	public static int Flags=0xC;
	public static int Flags2=0x10;
	public static int Unknown1=0x14;
	public static int Unknown2=0x18;
	public static int PlaybackSpeed=0x1C;
	public static int BoundingBox=0x20;//float[6]
	public static int Radius=0x38;
	public static int NextAnimation=0x3C;
	public static int Index=0x3E;
	public static int end=0x40;
	
	public AnimSequ(ByteBuffer databuffer){
		super(databuffer);

		buff.limit( end );
		
		databuffer.position( databuffer.position() + buff.limit());
	}
	
	public AnimSequ(){
		super(end);
		buff.putInt(Flags,0x20);
		buff.putInt(Flags2,0x7fff);
		buff.putShort(NextAnimation,(short) 0xffff);
		buff.position(0);
	}
	
	public void render(){
		buff.position(0);
	}
	
	/**
	 * Returns the AnimationID
	 * 
	 * @return short
	 */
	public short getAnimID(){
		return buff.getShort(AnimationID);
	}
	/**
	 * Sets the AnimationID
	 * 
	 * @param val short
	 */
	public void setAnimID(short val){
		buff.putShort(AnimationID, val);
	}
	
	/**
	 * Returns the SubAnimationID
	 * 
	 * @return short
	 */
	public short getSubAnimID(){
		return buff.getShort(SubAnimationID);
	}
	/**
	 * Sets the SubAnimationID
	 * 
	 * @param val short
	 */
	public void setSubAnimID(short val){
		buff.putShort(SubAnimationID, val);
	}
}