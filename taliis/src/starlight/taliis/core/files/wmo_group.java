package starlight.taliis.core.files;

import java.io.InvalidClassException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.wmo.group.*;

/**
 * This is the implementation of a wmo_group file
 * 
 * 
 * @author Tigurius
 *
 */

public class wmo_group extends wmo {
	public MVER mver;
	public MOGP mogp;
	public MOPY mopy;
	public MOVI movi;
	public MOVT movt;
	public MONR monr;
	public MOTV motv;
	public MOBA moba;
	public MOLR molr;
	public MODR modr;
	public MOBN mobn;
	public MOBR mobr;
	public MOCV mocv;
	public unkChunk mliq;
	
	/**
	 * Load and Init the WMO_Group structure
	 * @throws ChunkNotFoundException 
	 */
	public wmo_group(ByteBuffer databuffer) throws InvalidClassException, ChunkNotFoundException {
		super(databuffer);
		parse(databuffer);
	}
	
	/**
	 * Creates a new WMO_Group object
	 * -> not impementated!
	 */
	public wmo_group() {
		create();
	}
	
	/**
	 * Loads the data and init the chunk structures
	 * @throws ChunkNotFoundException 
	 */

	private void parse(ByteBuffer databuffer) throws InvalidClassException {

		// get version

		try {
			mver = new MVER(databuffer);
			mogp = new MOGP(databuffer);
			mopy = new MOPY(databuffer);
			movi = new MOVI(databuffer);
			movt = new MOVT(databuffer);
			monr = new MONR(databuffer);
			motv = new MOTV(databuffer);
			moba = new MOBA(databuffer);
			if(chunk.nextChunk(databuffer).compareTo("RLOM")==0)
				molr = new MOLR(databuffer);
			if(chunk.nextChunk(databuffer).compareTo("RDOM")==0)
				modr = new MODR(databuffer);
			mobn = new MOBN(databuffer);
			mobr = new MOBR(databuffer);
			if(databuffer.hasRemaining()
					   && chunk.nextChunk(databuffer).compareTo("VCOM")==0) mocv = new MOCV(databuffer);
			if(databuffer.hasRemaining()
					   && chunk.nextChunk(databuffer).compareTo("QILM")==0) mliq = new unkChunk(databuffer, "QLIM");
		} catch(ChunkNotFoundException e) {
			throw new InvalidClassException(e.getMessage());
		}


	}
	/**
	 * Creates a new wmo_group file
	 * NOT IMPLEMENTATED RIGHT NOW!
	 */
	private void create(){
		mver = new MVER();
		mogp = new MOGP();
		mopy = new MOPY();
		movi = new MOVI();
		movt = new MOVT();
		monr = new MONR();
		motv = new MOTV();
		moba = new MOBA();
		moba.setnewSize(1, false);
		moba.entrys[0]=new MOBA_Entry((short)0,(short)0,0,(short) 0);
		//molr = new MOLR();
		modr = new MODR();
		mogp.setFlag(0x809);
		mobn = new MOBN();
		mobn.setnewSize(1, false);
        mobn.entrys[0]=new MOBN_Entry(0, 0);
		mobr = new MOBR();
		//mocv = new MOCV();
		
	}
	
// -------------------------------------------------------	

	/**
	 * Renders the whole data and prepare everything
	 * for save routines (set offsets and so on)
	 */
	public void render() {
		
		/*
		 * First of all - fixing of different shit which 
		 * may has been corrupted
		 */
		if(mocv!=null&&mocv.count!=movt.nVertices){
			mocv.setnewsize(movt.nVertices, false);
			mocv.render();
			byte[] tempcol={0,0,0,(byte) 0xff};
			for(int i=0;i<movt.nVertices;i++)
				mocv.setColor(i, tempcol);
		}
		if(monr!=null&&monr.nNormals!=movt.nVertices){
			monr.setnewSize(movt.nVertices, false);
			monr.render();
			float[] temppos={0.0f,0.0f,0.0f};
			for(int i=0;i<movt.nVertices;i++)
				monr.setnewNormalPos(i, temppos);
		}
		if(mopy!=null&&movi.nTriangles!=mopy.count){
			mopy.setnewsize(movi.nTriangles, false);
			mopy.render();
			for(int i=0;i<movi.nTriangles;i++){
				mopy.setflag(i, (byte) 0x48, true);
				mopy.setmatid(i, (byte) 0, true);
			}
		}
		if(motv!=null&&movt.nVertices!=motv.nTexVertices){
			motv.setnewSize(movt.nVertices, false);
			motv.render();
			float[] temppos={0.0f,0.0f};
			for(int i=0;i<movt.nVertices;i++)
				motv.setnewTexVertex(i, temppos);
		}
		
		/*
		 * Now the real rendering function ^.^
		 */
		int ro = 0; // running offset
		
		mver.render();
		ro+=mver.getSize();
		
		
		mogp.render();
		ro+=mogp.getSize();
		
		mopy.render();
		ro+=mopy.getSize();
		
		
		movi.render();
		ro+=movi.getSize();
		
		movt.render();
		ro+=movt.getSize();
		
		monr.render();
		ro+=monr.getSize();
		
		motv.render();
		ro+=motv.getSize();
		
		moba.render();
		ro+=moba.getSize();
		
		if(molr!=null){
		molr.render();
		ro+=molr.getSize();
		}
		
		if(modr!=null){
		modr.render();
		ro+=modr.getSize();
		}
		
		mobn.render();
		ro+=mobn.getSize();
		
		mobr.render();
		ro+=mobr.getSize();
		
		if(mocv!=null){
		mocv.render();
		ro+=mocv.getSize();
		}
		
		if(mliq!=null){
		mliq.render();
		ro+=mliq.getSize();
		}
		
		mogp.setSize(ro-0x15);
		mogp.render();
		// malloc new memory
		buff = ByteBuffer.allocate(ro);
		buff.order(ByteOrder.LITTLE_ENDIAN);
		buff.position(0);
		
		buff.put(mver.buff);
		buff.put(mogp.buff);
		buff.put(mopy.buff);
		buff.put(movi.buff);
		buff.put(movt.buff);
		buff.put(monr.buff);
		buff.put(motv.buff);
		buff.put(moba.buff);
		
		if(molr!=null)
		buff.put(molr.buff);
		
		if(modr!=null)
		buff.put(modr.buff);
		
		
		buff.put(mobn.buff);
		buff.put(mobr.buff);
		
		if(mocv!=null)
		buff.put(mocv.buff);

		if(mliq!=null)
		buff.put(mliq.buff);
		
		buff.position(0);
	}
	
// -------------------------------------------------------	
	
}
