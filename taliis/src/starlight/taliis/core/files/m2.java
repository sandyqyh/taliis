package starlight.taliis.core.files;

import java.io.InvalidClassException;
import java.nio.ByteBuffer;

import starlight.taliis.core.ZeroTerminatedString;
import starlight.taliis.core.memory;
import starlight.taliis.core.binary.m2.AnimationBlock;
import starlight.taliis.core.binary.m2.AnimSubStructure;
import starlight.taliis.core.binary.m2.Header;
import starlight.taliis.core.binary.m2.attachment.attachmentblock1;
import starlight.taliis.core.binary.m2.camera.Camerablock;
import starlight.taliis.core.binary.m2.effects.particleemitters;
import starlight.taliis.core.binary.m2.effects.ribbons;
import starlight.taliis.core.binary.m2.geometry.Bones;
import starlight.taliis.core.binary.m2.geometry.Renderflags;
import starlight.taliis.core.binary.m2.geometry.TexDef;
import starlight.taliis.core.binary.m2.geometry.Vertex;
import starlight.taliis.core.binary.m2.skeleton.AnimSequ;

/**
 * Well well the m2 model format ...
 * http://www.madx.dk/wowdev/wiki/index.php?title=M2#Effects
 * 
 * UNCOMPLETE !!!!
 * @author tharo
 *
 */

public class m2 extends wowfile {	
	public Header header=null;
	public ZeroTerminatedString modelname=null;
	//blocks
	public AnimSequ anims[]=null;
	public TexDef texdefs[]=null;
	public ZeroTerminatedString textures[]=null;
	public AnimationBlock transparency[]=null;
	public Camerablock camerablock[]=null;
	public Vertex vertex[]=null;
	
	public float[][] boundingvertices=null;
	public short[][] boundingtriangles=null;
	public float[][] boundingnormals=null;
	
	//theboneblock+values
	public Bones bone[]=null;
	public float[][] boneTranslation=null;//3*float
	public Quaternion[] boneRotation=null;
	public float[][] boneScaling=null;
	
	
	public attachmentblock1 attachments[]=null;
	
	
	public particleemitters particles[]=null;
	public float particlecolor[][];
	public short[][] particleopacity;
	public float particlesizes[][];
	
	public ribbons ribbon[]=null;
	public int[][] ribbonreftex=null;
	public int[][] ribbonintegers=null;
	public float[][][][] ribboncolor=null;
	public short[][][] ribbonopacity=null;
	public float[][][] ribbonabove=null;
	public float[][][] ribbonbelow=null;
	public int[][][] ribbonunk1=null;
	public int[][][] ribbonunk2=null;
	
	public short texunits[]=null;
	public short texreplace[]=null;
	public Renderflags renderflags[]=null;
	
	//lookuptables
	public short bonelookup[]=null;
	public short animlookup[]=null;
	public short texturelookup[]=null;
	public short texanimlookup[]=null;
	public short keybonelookup[]=null;
	
	//data
	public float translationpos[][];
	public float translationtar[][];
	
	
	
	//internal stuff
	boolean created=false;
	/**
	 * constructor for opening files
	 * @param databuffer ByteBuffer of the data stream
	 */
	public m2(ByteBuffer databuffer) throws InvalidClassException {		
		super(databuffer);
		
		load(databuffer);
		
		System.out.println("Stopped at: 0x" + 
				Long.toHexString(databuffer.position()));
	}
	
	
	public m2(){
		created=true;
		
		header=new Header();
		modelname=new ZeroTerminatedString("Test");
		anims=new AnimSequ[1];
		anims[0]=new AnimSequ();
		bone=new Bones[1];
		bone[0]=new Bones();
		bonelookup=new short[1];
		bonelookup[0]=0;
		
		keybonelookup=new short[1];
		keybonelookup[0]=-1;
		
		vertex=new Vertex[1];
		vertex[0]=new Vertex();

		transparency=new AnimationBlock[1];
		transparency[0]=new AnimationBlock();
		
		texdefs=new TexDef[1];
		texdefs[0]=new TexDef();
		texdefs[0].setType(0x0);
		
		textures=new ZeroTerminatedString[1];
		textures[0]=new ZeroTerminatedString("World\\Scale\\1_Null.blp");
		
		texturelookup=new short[1];
		texturelookup[0]=0;
		
		texunits=new short[1];
		texunits[0]=0;
		
		texreplace=new short[1];
		texreplace[0]=0;
		
		texanimlookup=new short[1];
		texanimlookup[0]=-1;
		
		renderflags=new Renderflags[1];
		renderflags[0]=new Renderflags();
	}

	
	void load(ByteBuffer databuffer) throws InvalidClassException {
		int nTextures;
		int ofsTextures;
		int nVertices, ofsVertices,nBones,ofsBones;
		int texlength[];
		int texoffs[];
		
		header = new Header(buff);
		
		
		// check magic
		if(header.getMagic().compareTo("MD20") != 0) {
			throw new InvalidClassException("Invalid m2 Data.");
		}
		
		// check version
		byte version[] = header.getVersion();
		
		//08 01 00 00 Wotlk 1.8 files
		if(version[0]!=8 || version[1]!=1) {
			System.err.print("Invalid Version found: " +
					version[1] + "." + 
					version[0] + "\t"
			);
			
			for(int i=0; i<4; i++)
				System.err.print(version[i] + " ");
			
			System.err.println();
			//return;
		}
		
		
		
		
		// read in stuff ..
		buff.position(header.getofsName());
		modelname=new ZeroTerminatedString(buff);
		
		/*
		# 3 Skeleton and animation

		    * 3.1 Standard animation block
		    * 3.2 Global sequences
		    * 3.3 Animation sequences
		    * 3.4 Bones
		
		# 4 Geometry and rendering
		
		    * 4.1 Vertices
		    * 4.2 Views (LOD)
		          o 4.2.1 Indices
		          o 4.2.2 Triangles
		          o 4.2.3 Vertex properties
		          o 4.2.4 Submeshes
		          o 4.2.5 Texture units
		    * 4.3 Render flags
		    * 4.4 Texture unit lookup table
		    * 4.5 Colors and transparency
		          o 4.5.1 Colors
		          o 4.5.2 Transparency lookup table
		          o 4.5.3 Transparency
		    * 4.6 Textures
		          o 4.6.1 Texture lookup table
		          o 4.6.2 Texture definitions
		          o 4.6.3 Texture animation lookup table
		          o 4.6.4 Texture animations
		*/
		
		
		anims=new AnimSequ[header.getNAnim()];
		buff.position(header.getAnimoffs());
		for(int i=0;i<header.getNAnim();i++){
			anims[i]=new AnimSequ(buff);
		}
		System.out.println("Loaded AnimSequences\n");
		
		nVertices= header.getnVertices();
		ofsVertices= header.getofsVertices();
		buff.position(ofsVertices);
		vertex=new Vertex[nVertices];
		for(int i=0;i<nVertices;i++){
			vertex[i]=new Vertex(buff);
		}
		System.out.println("Loaded Vertices\n");
		
		nBones= header.getnBones();
		ofsBones= header.getofsBones();
		buff.position(ofsBones);
		bone=new Bones[nBones];
		for(int i=0;i<nBones;i++){
			bone[i]=new Bones(buff);
		}
		System.out.println("Loaded Bones\n");
		buff.position(header.getofsBonelookup());
		bonelookup=new short[header.getnBonelookup()];
		for(int i=0;i<header.getnBonelookup();i++)
			bonelookup[i]=buff.getShort();
		
		keybonelookup=new short[header.getnKeyBoneLookup()];
		buff.position(header.getofsKeyBoneLookup());
		for(int i=0;i<header.getnKeyBoneLookup();i++){
			keybonelookup[i]=buff.getShort();
		}
		
		texturelookup=new short[header.getnTexLookup()];
		buff.position(header.getofsTexLookup());
		for(int i=0;i<header.getnTexLookup();i++){
			texturelookup[i]=buff.getShort();
		}
		texanimlookup=new short[header.getnTexAnimLookup()];
		buff.position(header.getofsTexAnimLookup());
		for(int i=0;i<header.getnTexAnimLookup();i++){
			texanimlookup[i]=buff.getShort();
		}
		
		nTextures= header.getnTextures();
		ofsTextures= header.getofsTextures();
		buff.position(ofsTextures);
		texdefs=new TexDef[nTextures];
		texlength=new int[nTextures];
		texoffs= new int[nTextures];
		for(int i=0;i<nTextures;i++){
			texdefs[i]=new TexDef(buff);
			texlength[i]=texdefs[i].getlenFilename();
			texoffs[i]=texdefs[i].getofsFilename();

		}
		
		textures= new ZeroTerminatedString[nTextures];
		for(int i=0;i<nTextures;i++){
				buff.position(0);
				buff.position(texoffs[i]);
				textures[i]=new ZeroTerminatedString(buff);
			}
		System.out.println("Loaded Textures\n");
		
		
		texunits=new short[header.getnTexUnits()];
		buff.position(header.getofsTexUnits());
		for(int i=0;i<header.getnTexUnits();i++){
			texunits[i]=buff.getShort();
		}
		texreplace=new short[header.getnTexReplace()];
		buff.position(header.getofsTexReplace());
		for(int i=0;i<header.getnTexReplace();i++){
			texreplace[i]=buff.getShort();
		}
		renderflags=new Renderflags[header.getnRenderFlags()];
		buff.position(header.getofsRenderFlags());
		for(int i=0;i<header.getnRenderFlags();i++){
			renderflags[i]=new Renderflags(buff);
		}
		//transpareny
		transparency=new AnimationBlock[header.getnTransparency()];
		buff.position(0);
		int ofsTrans =header.getofsTransparency();
		buff.position(ofsTrans);
		for(int i=0;i<header.getnTransparency();i++){
			transparency[i]=new AnimationBlock(buff);
		}
		for(int i=0;i<header.getnTransparency();i++){
			getTransparency(i);
		}
		
		System.out.println("Loaded Transparency\n");
		// 5 Effects
			// * 5.1 Ribbon emitters
			buff.position(header.getofsRibbons());
			ribbon=new ribbons[header.getnRibbons()];
			for(int i=0;i<header.getnRibbons();i++){
				ribbon[i]=new ribbons(buff);
			}
			
			ribbonreftex=new int[ribbon.length][];
			ribbonintegers=new int[ribbon.length][];
			ribboncolor=new float[ribbon.length][][][];
			ribbonopacity=new short[ribbon.length][][];
			ribbonabove=new float[ribbon.length][][];
			ribbonbelow=new float[ribbon.length][][];
			ribbonunk1=new int[ribbon.length][][];
			ribbonunk2=new int[ribbon.length][][];
			for(int i=0;i<header.getnRibbons();i++){
				ribbonreftex[i]=new int[ribbon[i].getnTexture()];
				buff.position(ribbon[i].getofsTextures());
				for(int k=0;k<ribbon[i].getnTexture();k++){
					ribbonreftex[i][k]=buff.getInt();
				}
				ribbonintegers[i]=new int[ribbon[i].getnInt()];
				for(int k=0;k<ribbon[i].getnInt();k++){
					ribbonintegers[i][k]=buff.getInt();
				}
				AnimSubStructure temp[];
				
				ribboncolor[i]=new float[ribbon[i].Color.getnKeySubstructures()][][];
				temp=new AnimSubStructure[ribbon[i].Color.getnKeySubstructures()];
				buff.position(ribbon[i].Color.getofsKeySubstructures());
				for(int k=0;k<ribbon[i].Color.getnKeySubstructures();k++){
					temp[k]=new AnimSubStructure(buff);
				}
				for(int k=0;k<ribbon[i].Color.getnKeySubstructures();k++){
					ribboncolor[i][k]=new float[temp[k].getnValues()][3];
					buff.position(temp[k].getofsValues());
					for(int j=0;j<temp[k].getnValues();j++){
						for(int n=0;n<3;n++){
						ribboncolor[i][k][j][n]=buff.getFloat();
						
						}
					}
				}
				
				ribbonopacity[i]=new short[ribbon[i].Opacity.getnKeySubstructures()][];
				temp=new AnimSubStructure[ribbon[i].Opacity.getnKeySubstructures()];
				buff.position(ribbon[i].Opacity.getofsKeySubstructures());
				for(int k=0;k<ribbon[i].Opacity.getnKeySubstructures();k++){
					temp[k]=new AnimSubStructure(buff);
				}
				for(int k=0;k<ribbon[i].Opacity.getnKeySubstructures();k++){
					ribbonopacity[i][k]=new short[temp[k].getnValues()];
					buff.position(temp[k].getofsValues());
					for(int j=0;j<temp[k].getnValues();j++){
						ribbonopacity[i][k][j]=buff.getShort();
					
					}
				}
				
				ribbonabove[i]=new float[ribbon[i].Above.getnKeySubstructures()][];
				temp=new AnimSubStructure[ribbon[i].Above.getnKeySubstructures()];
				buff.position(ribbon[i].Above.getofsKeySubstructures());
				for(int k=0;k<ribbon[i].Above.getnKeySubstructures();k++){
					temp[k]=new AnimSubStructure(buff);
				}
				for(int k=0;k<ribbon[i].Above.getnKeySubstructures();k++){
					ribbonabove[i][k]=new float[temp[k].getnValues()];
					buff.position(temp[k].getofsValues());
					for(int j=0;j<temp[k].getnValues();j++){
						ribbonabove[i][k][j]=buff.getFloat();
					
					}
				}
				
				ribbonbelow[i]=new float[ribbon[i].Below.getnKeySubstructures()][];
				temp=new AnimSubStructure[ribbon[i].Below.getnKeySubstructures()];
				buff.position(ribbon[i].Below.getofsKeySubstructures());
				for(int k=0;k<ribbon[i].Below.getnKeySubstructures();k++){
					temp[k]=new AnimSubStructure(buff);
				}
				for(int k=0;k<ribbon[i].Below.getnKeySubstructures();k++){
					ribbonbelow[i][k]=new float[temp[k].getnValues()];
					buff.position(temp[k].getofsValues());
					for(int j=0;j<temp[k].getnValues();j++){
						ribbonbelow[i][k][j]=buff.getFloat();
					
					}
				}
				
				ribbonunk1[i]=new int[ribbon[i].Unk1.getnKeySubstructures()][];
				temp=new AnimSubStructure[ribbon[i].Unk1.getnKeySubstructures()];
				buff.position(ribbon[i].Unk1.getofsKeySubstructures());
				for(int k=0;k<ribbon[i].Unk1.getnKeySubstructures();k++){
					temp[k]=new AnimSubStructure(buff);
				}
				for(int k=0;k<ribbon[i].Unk1.getnKeySubstructures();k++){
					ribbonunk1[i][k]=new int[temp[k].getnValues()];
					buff.position(temp[k].getofsValues());
					for(int j=0;j<temp[k].getnValues();j++){
						ribbonunk1[i][k][j]=buff.getInt();
					
					}
				}
				
				ribbonunk2[i]=new int[ribbon[i].Unk2.getnKeySubstructures()][];
				temp=new AnimSubStructure[ribbon[i].Unk2.getnKeySubstructures()];
				buff.position(ribbon[i].Unk2.getofsKeySubstructures());
				for(int k=0;k<ribbon[i].Unk2.getnKeySubstructures();k++){
					temp[k]=new AnimSubStructure(buff);
				}
				for(int k=0;k<ribbon[i].Unk2.getnKeySubstructures();k++){
					ribbonunk2[i][k]=new int[temp[k].getnValues()];
					buff.position(temp[k].getofsValues());
					for(int j=0;j<temp[k].getnValues();j++){
						ribbonunk2[i][k][j]=buff.getInt();
					
					}
				}
			}
		System.out.println("Loaded Ribbons\n");
			// * 5.2 Particle emitters
		
		particles=new particleemitters[header.getnParticles()];
		buff.position(header.getofsParticles());
		for(int i=0;i<header.getnParticles();i++){
			particles[i]=new particleemitters(buff);
		}
		//the values of the fakeanimblocks first as they're the easy
		particlecolor=new float[header.getnParticles()][];
		for(int i=0;i<header.getnParticles();i++){
			particlecolor[i]=new float[particles[i].particlecolor.getnKeys()];
			buff.position(particles[i].particlecolor.getofsKeys());
			for(int j=0;j<particles[i].particlecolor.getnKeys();j++){
				particlecolor[i][j]=buff.getFloat();
				//System.out.println("Color of Particle "+i+" number "+j+" is\t"+particlecolor[i][j]);
			}
		}
		particleopacity=new short[header.getnParticles()][];
		for(int i=0;i<header.getnParticles();i++){
			particleopacity[i]=new short[particles[i].particleopacity.getnKeys()];
			buff.position(particles[i].particleopacity.getofsKeys());
			for(int j=0;j<particles[i].particleopacity.getnKeys();j++){
				particleopacity[i][j]=buff.getShort();
				//System.out.println("Opacity number " +j+ " of Particle "+i+" is\t"+particleopacity[i][j]);
			}
		}
		particlesizes=new float[header.getnParticles()][];
		for(int i=0;i<header.getnParticles();i++){
			particlesizes[i]=new float[particles[i].particlesizes.getnKeys()];
			buff.position(particles[i].particlesizes.getofsKeys());
			for(int j=0;j<particles[i].particlesizes.getnKeys();j++){
				particlesizes[i][j]=buff.getFloat();
				//System.out.println("Size of Particle "+i+" number "+j+" is\t"+particlesizes[i][j]);
			}
		}
		
		
		/**
		 * Well let's start the Camerablock :O
		 */
		camerablock=new Camerablock[header.getnCameras()];
		//the interesting values..
		translationpos=new float[header.getnCameras()][];
		translationtar=new float[header.getnCameras()][];
		buff.position(0);
		buff.position(header.getofsCameras());
		for(int i=0;i<header.getnCameras();i++)
		{
			camerablock[i]=new Camerablock(buff);
			/*translationtar[i]=new float[camerablock[i].TransTar.getnKeySubstructures()];
			translationpos[i]=new float[camerablock[i].TransPos.getnKeySubstructures()];
			buff.position(0);
			buff.position(camerablock[i].TransPos.getofsKeySubstructures()+4);
			buff.position(buff.getInt());
			for(int j=0;j<camerablock[i].TransPos.getnKeySubstructures();j++){
			translationpos[i][j]=buff.getFloat();
			}
			buff.position(0);
			buff.position(camerablock[i].TransTar.getofsKeySubstructures()+4);
			buff.position(buff.getInt());
			for(int j=0;j<camerablock[i].TransTar.getnKeySubstructures();j++){
				translationtar[i][j]=buff.getFloat();
				}*/
		}
		System.out.println("Loaded Cameras\n");
		/*
		# 6 Miscellaneous
		
		    * 6.1 Bounding volumes
		    * 6.2 Lights
		    * 6.3 Cameras
		    * 6.4 Camera lookup table
		*/
		//Bounding-Volumes:
		boundingvertices=new float[header.getnBoundingVertices()][3];
		buff.position(header.getofsBoundingVertices());
		for(int i=0;i<header.getnBoundingVertices();i++){
			for(int k=0;k<3;k++){
				boundingvertices[i][k]=buff.getFloat();
			}
		}
		boundingtriangles=new short[header.getnBoundingTriangles()][3];
		buff.position(header.getofsBoundingTriangles());
		for(int i=0;i<header.getnBoundingTriangles();i++){
			for(int k=0;k<3;k++){
				boundingtriangles[i][k]=buff.getShort();
			}
		}
		boundingnormals=new float[header.getnBoundingNormals()][3];
		buff.position(header.getofsBoundingNormals());
		for(int i=0;i<header.getnBoundingNormals();i++){
			for(int k=0;k<3;k++){
				boundingnormals[i][k]=buff.getFloat();
			}
		}
		/*
		# 7 Attachments
		
		    * 7.1 Block 1
		    * 7.2 Attachment Lookup
		    * 7.3 Block 2
		
		# 8 Unknown blocks
		
		    * 8.1 Block C
		    * 8.2 Block D
		    * 8.3 Block F
		    * 8.4 Block K - Replacable texture lookup
		    * 8.5 Block Y
		 */
		
		//attachment
		attachments=new attachmentblock1[header.getnAttachment()];
		buff.position(header.getofsAttachment());
		for(int i=0;i<header.getnAttachment();i++){
			attachments[i]=new attachmentblock1(buff);
		}
		System.out.println("Loaded Attachmentblock 1 \t");

	}
	
	

	/**
	 * get the texture value
	 * @param number index in the string array
	 * @return the texture filename
	 */
	public String getString(int index) {
		if(textures==null) return null;
		if(index<0 || index>textures.length) return null;
		return textures[index].toString();
	}
	
	public void setnVertices(int val){
		vertex=new Vertex[val];
		for(int i=0;i<val;i++){
			vertex[i]=new Vertex();
		}
	}
	
	public void setnBones(int val){
		bone=new Bones[val];
		for(int i=0;i<val;i++){
			bone[i]=new Bones();
		}
	}
	
	/*
	 * Boundingfunctions xD
	 */
	public void setnBoundingVertices(int val){
		boundingvertices=new float[val][3];
	}
	public void setBoundingVertice(int nr,float[] val){
		for(int i=0;i<3;i++){
			boundingvertices[nr][i]=val[i];
		}
	}
	public void setnBoundingNormals(int val){
		boundingnormals=new float[val][3];
	}
	public void setBoundingNormal(int nr,float[] val){
		for(int i=0;i<3;i++){
			boundingnormals[nr][i]=val[i];
		}
	}
	public void setnBoundingTriangles(int val){
		boundingtriangles=new short[val][3];
	}
	public void setBoundingTriangle(int nr,short[] val){
		for(int i=0;i<3;i++){
			boundingtriangles[nr][i]=val[i];
		}
	}
	
	
	public void setnAttachments(int val){
		attachments=new attachmentblock1[val];
		for(int i=0;i<val;i++){
			attachments[i]=new attachmentblock1();
		}
	}
	
	public int getAttachBone(int nr){
		return attachments[nr].getBone();
	}
	public void setAttachBone(int nr,int val){
		attachments[nr].setBone(val);
	}
	
	public short[][] getTransparency(int n){
		short[][] s = null;
		buff.position(transparency[n].getofsKeySubstructures());
		AnimSubStructure temp[] = new AnimSubStructure[transparency[n].getnKeySubstructures()];
		for(int i=0;i<transparency[n].getnKeySubstructures();i++){
			temp[i]=new AnimSubStructure(buff);
		}
		s=new short[transparency[n].getnKeySubstructures()][];
		for(int i=0;i<transparency[n].getnKeySubstructures();i++){
		if(temp[i].getnValues()!=0||temp[i].getofsValues()!=0){
			buff.position(temp[i].getofsValues());
			
		s[i]=new short[temp[i].getnValues()];
		for(int j=0;j<temp[i].getnValues();j++){
			s[i][j]=buff.getShort();
			System.out.println(s[i][j]);
		}
		
		}
		else return null;
		}
		return s;
	}
	
	public void setTransparency(short val,int n){
		buff.position(transparency[n].getofsKeySubstructures());
		AnimSubStructure temp[] = new AnimSubStructure[transparency[n].getnKeySubstructures()];
		for(int i=0;i<transparency[n].getnKeySubstructures();i++){
			temp[i]=new AnimSubStructure(buff);
		}
		buff.position(temp[n].getofsValues());
		for(int i=0;i<temp[n].getnValues();i++) buff.putShort(val);
	}
	
	public void setModelName(String name){
		modelname=new ZeroTerminatedString(name);
	}
	
	public void addTexture(String texname){
		TexDef[] tmp=texdefs;
		texdefs=new TexDef[texdefs.length+1];
		for(int i=0;i<tmp.length;i++){
			texdefs[i]=tmp[i];
		}
		texdefs[tmp.length]=new TexDef();
		ZeroTerminatedString[] temp=textures;
		textures=new ZeroTerminatedString[textures.length+1];
		for(int i=0;i<temp.length;i++){
			textures[i]=temp[i];
		}
		textures[temp.length]=new ZeroTerminatedString(texname);
		texturelookup=new short[texturelookup.length+1];
		for(int k=0;k<texturelookup.length;k++){
			texturelookup[k]=(short)k;
		}
	}
	
	

	
	/**
	 * We don't use the size vars of eg Short(==Short-Size)
	 * as they return the size in Bits but we use Bytes!
	 * and we now the sizes already so no need of any fucking calculation no more
	 * short=2 Bytes, float&int=4 Bytes
	 * 
	 * @return size needed
	 */
	int calcSize(){
		int tmp=0;
		if(header!=null)
		tmp+=Header.end;
		tmp+=0x20;
		if(anims!=null)
		tmp+=AnimSequ.end*anims.length+calcLine(anims.length,AnimSequ.end);
		if(bone!=null){
		tmp+=Bones.end*bone.length+calcLine(bone.length,Bones.end);
		tmp+=3*(AnimSubStructure.end*bone.length+calcLine(bone.length,AnimSubStructure.end));
		tmp+=bone.length*(3*4)+calcLine(bone.length,(3*4));
		tmp+=bone.length*Quaternion.end+calcLine(bone.length,Quaternion.end);
		tmp+=bone.length*(3*4)+calcLine(bone.length,(3*4));
		}
		if(bonelookup!=null)
		tmp+=bonelookup.length*2+calcLine(bonelookup.length,2);
		if(keybonelookup!=null){
			tmp+=keybonelookup.length*2+calcLine(keybonelookup.length,2);
		}
		if(vertex!=null)
		tmp+=Vertex.end*vertex.length+calcLine(vertex.length,Vertex.end);
		if(transparency!=null){
			tmp+=transparency.length*AnimationBlock.end+calcLine(transparency.length,AnimationBlock.end);
			tmp+=2*(AnimSubStructure.end*transparency.length)+calcLine(transparency.length,AnimSubStructure.end*2);
			tmp+=2*(2*transparency.length)+calcLine((transparency.length),4);//values
			tmp+=2*transparency.length+calcLine(transparency.length,2);//lookuptable
		}
		if(texdefs!=null){
			tmp+=texdefs.length*TexDef.end+calcLine(texdefs.length,TexDef.end);
		}
		if(textures!=null){
			for(int i=0;i<textures.length;i++){
				tmp+=textures[i].getLength()+calcLine(textures[i].getLength(),1);
			}
		}
		if(texturelookup!=null){
			tmp+=texturelookup.length*2+calcLine(texturelookup.length,2);
		}
		if(texunits!=null){
			tmp+=texunits.length*2+calcLine(texunits.length,2);
		}
		if(texreplace!=null){
			tmp+=texreplace.length*2+calcLine(texreplace.length,2);
		}
		if(texanimlookup!=null){
			tmp+=texanimlookup.length*2+calcLine(texanimlookup.length,2);
		}
		if(renderflags!=null){
			tmp+=renderflags.length*Renderflags.end+calcLine(renderflags.length,Renderflags.end);
		}
		if(boundingvertices!=null){
			tmp+=boundingvertices.length*3*4+calcLine(boundingvertices.length*3,4);
		}
		if(boundingnormals!=null){
			tmp+=boundingnormals.length*3*4+calcLine(boundingnormals.length*3,4);
		}
		if(boundingtriangles!=null){
			tmp+=boundingtriangles.length*3*2+calcLine(boundingtriangles.length*3,2);
		}
		if(attachments!=null){
			tmp+=attachments.length*attachmentblock1.end+calcLine(attachments.length,attachmentblock1.end);
		}
		if(ribbon!=null){
			tmp+=ribbon.length*ribbons.end+calcLine(ribbon.length,ribbons.end);
			tmp+=6*(AnimSubStructure.end*ribbon.length)+calcLine(6*AnimSubStructure.end,ribbon.length);
			tmp+=(2*ribbon.length)+calcLine(2,ribbon.length);//opacity
			tmp+=5*(4*ribbon.length)+5*calcLine(4,ribbon.length);//color*3+above+below
			tmp+=4*(4*ribbon.length)+4*calcLine(4,ribbon.length);//unk1,unk2,refint,reftex
		}
		
		return tmp;		
	}
	
	int calcLine(int n,int size){
		return 16-((size*n)%16);
	}
	
	ByteBuffer fillLine(ByteBuffer tmp){
		int Pos, linePos;
		Pos=tmp.position();
		linePos=Pos&0xFFFFFFF0;
		for(int i=0;i<16-(Pos-linePos);i++){
			tmp.put((byte) 0);
		}
		return tmp;
	}
	
	public void render(){
		//if(created){
			//running offset
			int ro=0;
			//Headersize+0x10Bytes for Name+AnimSequ+Bone+0x10 for bonelookup
			ByteBuffer tmp=doRebirth(calcSize());
			header=new Header();
			tmp.put(header.buff);
			fillLine(tmp);
			ro=tmp.position();
			header.setofsName(ro);
			header.setlName(modelname.toString().length()+1);//zeroterminated
			modelname.render();
			tmp.put(modelname.toString().getBytes());
			fillLine(tmp);
			ro=tmp.position();
			if(anims!=null){
			header.setAnimOffs(ro);
			header.setNAnim(anims.length);
			for(int i=0;i<anims.length;i++){
			anims[i].render();
			tmp.put(anims[i].buff);
			}
			fillLine(tmp);
			}
			ro=tmp.position();
			if(bone!=null){
			header.setBoneOffs(ro);
			header.setNBone(bone.length);
			for(int i=0;i<bone.length;i++){
				/*if(boneRotation!=null){
						bone[i].Rot.setnKeySubstructures(boneRotation.length);
						bone[i].Rot.setnTimeSubstructures(boneRotation.length);
				}*/
			bone[i].render();
			tmp.put(bone[i].buff);
			}
			fillLine(tmp);
			}
			ro=tmp.position();
			if(bonelookup!=null){
			header.setofsBonelookup(ro);
			header.setnBonelookup(bonelookup.length);
			for(int i=0;i<bonelookup.length;i++){
			tmp.putShort(bonelookup[i]);
			}
			fillLine(tmp);
			ro=tmp.position();
			}
			if(keybonelookup!=null){
				header.setnKeyBoneLookup(keybonelookup.length);
				header.setofsKeyBoneLookup(ro);
				for(int i=0;i<keybonelookup.length;i++){
					tmp.putShort(keybonelookup[i]);
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			ro=tmp.position();
			if(vertex!=null){
			header.setnVertices(vertex.length);
			header.setofsVertices(ro);
			for(int i=0;i<vertex.length;i++){
				vertex[i].render();
				tmp.put(vertex[i].buff);
			}
			fillLine(tmp);
			}
			ro=tmp.position();
			if(transparency!=null){
				header.setnTransparency(transparency.length);
				header.setofsTransparency(ro);
				for(int i=0;i<transparency.length;i++){
					transparency[i]=new AnimationBlock();
					transparency[i].setnTimeSubstructures(1);
					transparency[i].setofsTimeSubstructures(ro+(i*AnimSubStructure.end)+AnimationBlock.end*transparency.length+(16-(AnimationBlock.end*transparency.length)%16));
					transparency[i].setnKeySubstructures(1);
					transparency[i].setofsKeySubstructures(ro+(i*AnimSubStructure.end)+(transparency.length*AnimSubStructure.end)+(16-(transparency.length*AnimSubStructure.end)%16)+AnimationBlock.end*transparency.length+(16-(AnimationBlock.end*transparency.length)%16));
					transparency[i].render();
					tmp.put(transparency[i].buff);
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<transparency.length;i++){
					AnimSubStructure temp=new AnimSubStructure();
					temp.setnValues(1);
					temp.setofsValues(ro+i*Short.SIZE+AnimSubStructure.end*transparency.length+(16-(AnimSubStructure.end*transparency.length)%16)+AnimSubStructure.end*transparency.length+(16-(AnimSubStructure.end*transparency.length)%16));
					temp.render();
					tmp.put(temp.buff);
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<transparency.length;i++){
					AnimSubStructure temp=new AnimSubStructure();
					temp.setnValues(1);					//+(transparency.length*Short.SIZE)
					temp.setofsValues(ro+(AnimSubStructure.end*transparency.length)+(16-(AnimSubStructure.end*transparency.length)%16)+(i*Short.SIZE)+(16-(transparency.length*Short.SIZE)%16));
					temp.render();
					tmp.put(temp.buff);
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<transparency.length;i++){
					tmp.putShort((short) 0);
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<transparency.length;i++){
					tmp.putShort((short) 0x7fff);
				}
				fillLine(tmp);
				ro=tmp.position();
				header.setnTransLookup(transparency.length);
				header.setofsTransLookup(ro);
				for(int i=0;i<transparency.length;i++){
					tmp.putShort((short) i);
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			if(texdefs!=null){
				ro=tmp.position();
				header.setnTextures(texdefs.length);
				header.setofsTextures(ro);
				for(int i=0;i<texdefs.length;i++){
					int texofs=0;
					texofs+=ro+texdefs.length*TexDef.end+(16-(texdefs.length*TexDef.end)%0x10);
					for(int j=0;j<i;j++){
						texofs+=textures[j].toString().length()+1+(16-(textures[j].toString().length()+1)%0x10);
					}
					
					texdefs[i].setlenFilename(textures[i].toString().length()+1);
					texdefs[i].setofsFilename(texofs);
					texdefs[i].render();
					tmp.put(texdefs[i].buff);
				}
				fillLine(tmp);
				int[] texpos=new int[textures.length];
				for(int i=0;i<textures.length;i++){
					texpos[i]=tmp.position();
					textures[i].render();
					tmp.put(textures[i].toString().getBytes());
					tmp.put((byte) 0);
					fillLine(tmp);
				}
				ro=tmp.position();
			}
			if(texturelookup!=null){
				header.setnTexLookup(texturelookup.length);
				header.setofsTexLookup(ro);
				for(int i=0;i<texturelookup.length;i++){
					tmp.putShort(texturelookup[i]);
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			if(texunits!=null){
				header.setnTexUnits(texunits.length);
				header.setofsTexUnits(ro);
				for(int i=0;i<texunits.length;i++){
					tmp.putShort(texunits[i]);
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			if(texreplace!=null){
				header.setnTexReplace(texreplace.length);
				header.setofsTexReplace(ro);
				for(int i=0;i<texreplace.length;i++){
					tmp.putShort(texreplace[i]);
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			if(texanimlookup!=null){
				header.setnTexAnimLookup(texanimlookup.length);
				header.setofsTexAnimLookup(ro);
				for(int i=0;i<texanimlookup.length;i++){
					tmp.putShort(texanimlookup[i]);
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			if(renderflags!=null){
				header.setnRenderFlags(renderflags.length);
				header.setofsRenderFlags(ro);
				for(int i=0;i<renderflags.length;i++){
					renderflags[i].render();
					tmp.put(renderflags[i].buff);
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			if(boundingtriangles!=null){
				header.setnBoundingTriangles(boundingtriangles.length);
				header.setofsBoundingTriangles(ro);
				for(int i=0;i<boundingtriangles.length;i++){
					for(int k=0;k<3;k++){
						tmp.putShort(boundingtriangles[i][k]);
					}
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			
			if(boundingvertices!=null){
				header.setnBoundingVertices(boundingvertices.length);
				header.setofsBoundingVertices(ro);
				for(int i=0;i<boundingvertices.length;i++){
					for(int k=0;k<3;k++){
						tmp.putFloat(boundingvertices[i][k]);
					}
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			if(boundingnormals!=null){
				header.setnBoundingNormals(boundingnormals.length);
				header.setofsBoundingNormals(ro);
				for(int i=0;i<boundingnormals.length;i++){
					for(int k=0;k<3;k++){
						tmp.putFloat(boundingnormals[i][k]);
					}
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			if(attachments!=null){
				header.setnAttachment(attachments.length);
				header.setofsAttachment(ro);
				for(int i=0;i<attachments.length;i++){
					attachments[i].render();
					tmp.put(attachments[i].buff);
				}
				fillLine(tmp);
				ro=tmp.position();
				header.setnAttachmentlookup(attachments.length);
				header.setofsAttachmentlookup(ro);
				for(int i=0;i<attachments.length;i++){
					tmp.putShort((short) i);
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			
			
			if(ribbon!=null){
				header.setnRibbons(ribbon.length);
				header.setofsRibbons(ro);
				for(int i=0;i<ribbon.length;i++){
					ribbon[i].Color.setnTimeSubstructures(1);
					ribbon[i].Color.setofsTimeSubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16));
					ribbon[i].Opacity.setnTimeSubstructures(1);
					ribbon[i].Opacity.setofsTimeSubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+1*(ribbon.length*AnimSubStructure.end)+1*(16-(AnimSubStructure.end*ribbon.length)%16));
					ribbon[i].Above.setnTimeSubstructures(1);
					ribbon[i].Above.setofsTimeSubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+2*(ribbon.length*AnimSubStructure.end)+2*(16-(AnimSubStructure.end*ribbon.length)%16));
					ribbon[i].Below.setnTimeSubstructures(1);
					ribbon[i].Below.setofsTimeSubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+3*(ribbon.length*AnimSubStructure.end)+3*(16-(AnimSubStructure.end*ribbon.length)%16));
					ribbon[i].Unk1.setnTimeSubstructures(1);
					ribbon[i].Unk1.setofsTimeSubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+4*(ribbon.length*AnimSubStructure.end)+4*(16-(AnimSubStructure.end*ribbon.length)%16));
					ribbon[i].Unk2.setnTimeSubstructures(1);
					ribbon[i].Unk2.setofsTimeSubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+5*(ribbon.length*AnimSubStructure.end)+5*(16-(AnimSubStructure.end*ribbon.length)%16));
					
					ribbon[i].Color.setnKeySubstructures(1);
					ribbon[i].Color.setofsKeySubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+6*(ribbon.length*AnimSubStructure.end)+6*(16-(AnimSubStructure.end*ribbon.length)%16));
					ribbon[i].Opacity.setnKeySubstructures(1);
					ribbon[i].Opacity.setofsKeySubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+7*(ribbon.length*AnimSubStructure.end)+7*(16-(AnimSubStructure.end*ribbon.length)%16));
					ribbon[i].Above.setnKeySubstructures(1);
					ribbon[i].Above.setofsKeySubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+8*(ribbon.length*AnimSubStructure.end)+8*(16-(AnimSubStructure.end*ribbon.length)%16));
					ribbon[i].Below.setnKeySubstructures(1);
					ribbon[i].Below.setofsKeySubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+9*(ribbon.length*AnimSubStructure.end)+9*(16-(AnimSubStructure.end*ribbon.length)%16));
					ribbon[i].Unk1.setnKeySubstructures(1);
					ribbon[i].Unk1.setofsKeySubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+10*(ribbon.length*AnimSubStructure.end)+10*(16-(AnimSubStructure.end*ribbon.length)%16));
					ribbon[i].Unk2.setnKeySubstructures(1);
					ribbon[i].Unk2.setofsKeySubstructures(ro+(i*AnimSubStructure.end)+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+11*(ribbon.length*AnimSubStructure.end)+11*(16-(AnimSubStructure.end*ribbon.length)%16));
					
					int temp=0;
					ribbon[i].setnInt(ribbonintegers[i].length);					
					for(int k=0;k<i;k++){
						temp+=ribbonintegers[k].length*Integer.SIZE+(16-(ribbonintegers[k].length*Integer.SIZE)%16);
					}
					ribbon[i].setofsInt(ro+temp+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+12*(ribbon.length*AnimSubStructure.end)+12*(16-(AnimSubStructure.end*ribbon.length)%16));
					ribbon[i].setnTexture(ribbonreftex[i].length);					
					temp=0;
					for(int k=0;k<ribbon.length;k++){
						temp+=ribbonintegers[k].length*Integer.SIZE+(16-(ribbonintegers[k].length*Integer.SIZE)%16);
					}
					for(int k=0;k<ribbon.length;k++){
						temp+=ribbonreftex[k].length*Integer.SIZE+(16-(ribbonreftex[k].length*Integer.SIZE)%16);
					}
					ribbon[i].setofsTextures(ro+temp+ribbons.end*ribbon.length+(16-(ribbons.end*ribbon.length)%16)+12*(ribbon.length*AnimSubStructure.end)+12*(16-(AnimSubStructure.end*ribbon.length)%16));
					
					ribbon[i].render();
					tmp.put(ribbon[i].buff);
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int k=0;k<6;k++){
				for(int i=0;i<ribbon.length;i++){
					AnimSubStructure temp=new AnimSubStructure();
					temp.render();
					tmp.put(temp.buff);
				}
				fillLine(tmp);
				}
				ro=tmp.position();
				int calc=0;
				for(int k=0;k<ribbon.length;k++){
					calc+=ribbonintegers[k].length*Integer.SIZE+(16-(ribbonintegers[k].length*Integer.SIZE)%16);
				}
				for(int k=0;k<ribbon.length;k++){
					calc+=ribbonreftex[k].length*Integer.SIZE+(16-(ribbonreftex[k].length*Integer.SIZE)%16);
				}
				for(int i=0;i<ribbon.length;i++){
					AnimSubStructure temp=new AnimSubStructure();
					temp.setnValues(1);
					temp.setofsValues(calc+ro
							+i*3*Float.SIZE+
							6*(AnimSubStructure.end*ribbon.length)+6*(16-(AnimSubStructure.end*ribbon.length)%16)+AnimSubStructure.end*transparency.length+(16-(AnimSubStructure.end*transparency.length)%16));
					temp.render();
					tmp.put(temp.buff);
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<ribbon.length;i++){
					AnimSubStructure temp=new AnimSubStructure();
					temp.setnValues(1);
					temp.setofsValues(ro+calc+
							i*Short.SIZE
							+ribbon.length*3*Float.SIZE+(16-(ribbon.length*3*Float.SIZE)%16)+6*(AnimSubStructure.end*ribbon.length)+6*(16-(AnimSubStructure.end*ribbon.length)%16)+AnimSubStructure.end*transparency.length+(16-(AnimSubStructure.end*transparency.length)%16));
					temp.render();
					tmp.put(temp.buff);
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<ribbon.length;i++){
					AnimSubStructure temp=new AnimSubStructure();
					temp.setnValues(1);
					temp.setofsValues(ro+calc
							+i*Float.SIZE
							+ribbon.length*Short.SIZE+(16-(ribbon.length*Short.SIZE)%16)+ribbon.length*3*Float.SIZE+(16-(ribbon.length*3*Float.SIZE)%16)+6*(AnimSubStructure.end*ribbon.length)+6*(16-(AnimSubStructure.end*ribbon.length)%16)+AnimSubStructure.end*transparency.length+(16-(AnimSubStructure.end*transparency.length)%16));
					temp.render();
					tmp.put(temp.buff);
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<ribbon.length;i++){
					AnimSubStructure temp=new AnimSubStructure();
					temp.setnValues(1);
					temp.setofsValues(ro+calc
							+i*Float.SIZE+ribbon.length*Float.SIZE+(16-(ribbon.length*Float.SIZE)%16)+ribbon.length*Short.SIZE+(16-(ribbon.length*Short.SIZE)%16)+ribbon.length*3*Float.SIZE+(16-(ribbon.length*3*Float.SIZE)%16)+6*(AnimSubStructure.end*ribbon.length)+6*(16-(AnimSubStructure.end*ribbon.length)%16)+AnimSubStructure.end*transparency.length+(16-(AnimSubStructure.end*transparency.length)%16));
					temp.render();
					tmp.put(temp.buff);
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<ribbon.length;i++){
					AnimSubStructure temp=new AnimSubStructure();
					temp.setnValues(1);
					temp.setofsValues(ro+calc
							+i*Integer.SIZE+2*ribbon.length*Float.SIZE+2*(16-(ribbon.length*Float.SIZE)%16)+ribbon.length*Short.SIZE+(16-(ribbon.length*Short.SIZE)%16)+ribbon.length*3*Float.SIZE+(16-(ribbon.length*3*Float.SIZE)%16)+6*(AnimSubStructure.end*ribbon.length)+6*(16-(AnimSubStructure.end*ribbon.length)%16)+AnimSubStructure.end*transparency.length+(16-(AnimSubStructure.end*transparency.length)%16));
					temp.render();
					tmp.put(temp.buff);
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<ribbon.length;i++){
					AnimSubStructure temp=new AnimSubStructure();
					temp.setnValues(1);
					temp.setofsValues(ro+calc
							+i*Integer.SIZE+ribbon.length*Integer.SIZE+(16-(ribbon.length*Integer.SIZE)%16)+2*ribbon.length*Float.SIZE+2*(16-(ribbon.length*Float.SIZE)%16)+ribbon.length*Short.SIZE+(16-(ribbon.length*Short.SIZE)%16)+ribbon.length*3*Float.SIZE+(16-(ribbon.length*3*Float.SIZE)%16)+6*(AnimSubStructure.end*ribbon.length)+6*(16-(AnimSubStructure.end*ribbon.length)%16)+AnimSubStructure.end*transparency.length+(16-(AnimSubStructure.end*transparency.length)%16));
					temp.render();
					tmp.put(temp.buff);
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<ribbon.length;i++){
					for(int k=0;k<ribbonintegers[i].length;k++){
						buff.putInt(ribbonintegers[i][k]);
					}
					fillLine(tmp);
					ro=tmp.position();
				}
				for(int i=0;i<ribbon.length;i++){
					for(int k=0;k<ribbonreftex[i].length;k++){
						buff.putInt(ribbonreftex[i][k]);
					}
					fillLine(tmp);
					ro=tmp.position();
				}
				for(int i=0;i<ribbon.length;i++){
					for(int k=0;k<ribboncolor[i].length;k++){
						for(int n=0;n<3;n++){
						System.out.println("Float:\t"+n+"\tk\t"+k+"\tribbon\t"+i);
						buff.putFloat(ribboncolor[i][k][0][n]);
						}
					}
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<ribbon.length;i++){
					for(int k=0;k<ribbonopacity[i].length;k++){
						buff.putShort(ribbonopacity[i][k][0]);
					}
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<ribbon.length;i++){
					for(int k=0;k<ribbonabove[i].length;k++){
						buff.putFloat(ribbonabove[i][k][0]);
					}
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<ribbon.length;i++){
					for(int k=0;k<ribbonbelow[i].length;k++){
						buff.putFloat(ribbonbelow[i][k][0]);
					}
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<ribbon.length;i++){
					for(int k=0;k<ribbonunk1[i].length;k++){
						buff.putInt(ribbonunk1[i][k][0]);
					}
				}
				fillLine(tmp);
				ro=tmp.position();
				for(int i=0;i<ribbon.length;i++){
					for(int k=0;k<ribbonunk2[i].length;k++){
						buff.putInt(ribbonunk2[i][k][0]);
					}
				}
				fillLine(tmp);
				ro=tmp.position();
			}
			
			//we need atleast one skin file
			header.setnViews(1);
			tmp.position(0);
			header.render();
			tmp.put(header.buff);
			buff = doRebirth(tmp.capacity());
			buff=tmp;
			buff.position(0);
		/*}
		else
		buff.position(0);*/
	}
}

/**
 * WoW-Quaternions consist of 4*short
 *
 */
class Quaternion extends memory{
	private final short x0,x1,x2,x3;
	public static final int end=0x8;
	
	public Quaternion(ByteBuffer databuffer){
		super(databuffer);
		buff.limit(end);
		x0=buff.getShort();
		x1=buff.getShort();
		x2=buff.getShort();
		x3=buff.getShort();
		databuffer.position( databuffer.position() + buff.limit());
	}
	public Quaternion(){
		super(end);
		x0=0;
		x1=0;
		x2=0;
		x3=0;
	}
	public void render(){
		buff.position(0);
		buff.putShort(x0);
		buff.putShort(x1);
		buff.putShort(x2);
		buff.putShort(x3);
		buff.position(0);
	}
	
	public Quaternion(short x0, short x1, short x2, short x3) {
        super(end);
		this.x0 = x0;
        this.x1 = x1;
        this.x2 = x2;
        this.x3 = x3;
    }
	
	 public String toString() {
	        return x0 + " + " + x1 + "i + " + x2 + "j + " + x3 + "k";
	    }
	 /*
	  * returns the 4 short values of the quaternion
	  */
	 public short[] giveValues(){
		 short[] tmp=new short[4];
		 tmp[0]=x0;
		 tmp[1]=x1;
		 tmp[2]=x2;
		 tmp[3]=x3;
		 return tmp;
	 }
	 
	 public double norm() {
	        return Math.sqrt(x0*x0 + x1*x1 +x2*x2 + x3*x3);
	    }

	    // return the quaternion conjugate
	    public Quaternion conjugate() {
	        return new Quaternion((short)x0,(short) -x1, (short)-x2,(short) -x3);
	    }
	 
	 public Quaternion plus(Quaternion b) {
	        Quaternion a = this;
	        return new Quaternion((short)(a.x0+b.x0),(short) (a.x1+b.x1),(short) (a.x2+b.x2),(short) (a.x3+b.x3));
	    }
	 
	// return a new Quaternion whose value is (this * b)
	    public Quaternion times(Quaternion b) {
	        Quaternion a = this;
	        short y0 = (short) (a.x0*b.x0 - a.x1*b.x1 - a.x2*b.x2 - a.x3*b.x3);
	        short y1 = (short) (a.x0*b.x1 + a.x1*b.x0 + a.x2*b.x3 - a.x3*b.x2);
	        short y2 = (short) (a.x0*b.x2 - a.x1*b.x3 + a.x2*b.x0 + a.x3*b.x1);
	        short y3 = (short) (a.x0*b.x3 + a.x1*b.x2 - a.x2*b.x1 + a.x3*b.x0);
	        return new Quaternion(y0, y1, y2, y3);
	    }

	    // return a new Quaternion whose value is the inverse of this
	    public Quaternion inverse() {
	        short d = (short) (x0*x0 + x1*x1 + x2*x2 + x3*x3);
	        return new Quaternion((short)(x0/d),(short)( -x1/d),(short) (-x2/d),(short) (-x3/d));
	    }


	    // return a / b
	    public Quaternion divides(Quaternion b) {
	         Quaternion a = this;
	        return a.inverse().times(b);
	    }
}
