package starlight.taliis.apps.wdttest;

import java.io.*;
import java.nio.*;
import starlight.taliis.core.files.*;
import starlight.taliis.core.chunks.*;
import java.nio.channels.*;


public class wdt_test {
	wdt obj;	// the open adt file were working with
	
	
	wdt_test() {

	}
	
	public void open(String FileName) {
		String strFile = FileName;
		
		try {
			// open the file an isolate the file channel
				File f = new File(FileName);
				RandomAccessFile fis = new RandomAccessFile(f, "rw");
				FileChannel fc = fis.getChannel();
				
			// create buffer (lill endian) with our files data
				int sz = (int)fc.size();
				ByteBuffer bb = fc.map(FileChannel.MapMode.READ_WRITE, 0, sz);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				
			// init wdt file
				obj = new wdt(bb);
				
			// clean up
				fc.close();
				fis.close();			
			
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}		
	}
	
	
	
	public static void main(String[] args) {
		if(args.length<1) {
			System.out.print("Simply WDT file test app \n"+
							"(c) 2007 by Tharo Herberg\n" +
							"Usage:\n\tjava wdt_test filename");
			System.exit(1);
		}
		
		wdt_test bla = new wdt_test();
		bla.open(args[0]);
		
		// print out all values of main chunk
		for(int i=0; i<64; i++) {
			for(int j=0; j<64; j++) {
				long a = bla.obj.main.getValue(i, j);
				//if(j==55 && i==43) System.out.print("X");
				//else 
				System.out.print( a );
			}
			System.out.println();
		}
		
		// new adt:
		for(int i=0; i<4; i++)
			for(int j=0; j<4; j++)
				bla.obj.main.setValue(43+i, 52+j, 1);
		
	}
}
