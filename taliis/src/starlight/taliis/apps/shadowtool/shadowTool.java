package starlight.taliis.apps.shadowtool;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import starlight.taliis.core.chunks.adt.MCSH;
import starlight.taliis.core.files.adt;
import starlight.taliis.helpers.fileLoader;

public class shadowTool {
	String inF, outF;
	
	public shadowTool(String inFile, String outFile) {
		inF = inFile;
		outF = outFile;
		
		if(inF.contains(".adt"))
			extract();
		else if(outF.contains(".adt"))
			imp();
	}
	
	private void extract() {
		System.out.println("Open " + inF);
		adt obj = new adt( fileLoader.openBuffer(inF) );
		
		// Setup output picture
		BufferedImage img = new BufferedImage(16*64, 16*64, BufferedImage.TYPE_BYTE_BINARY);
		
		for(int x=0; x<16; x++) {
			for(int y=0; y<16; y++) {
				// preload data
				int data[] = obj.mcnk[16*y+x].mcsh.getData();
				
				// confert to RGB BW picture ..
				for(int c=0; c<data.length; c++)
					if(data[c]!=0) data[c] = 0xFFFFFF;
			
				// set pixels	
				for(int c=0; c<64; c++)
					img.setRGB(x*64, //int startX,
			                   y*64 + c,//int startY,
			                   64,//int w,
			                   1,//int h,
			                   data,//int[] rgbArray,
			                   c*64,//int offset,
			                   64);//int scansize)
				
			}
		}
		
		File file = new File(outF);
        try {
			ImageIO.write(img, "png", file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void imp() {
		
	}
	
	
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String inFile="", outFile="";
		
		System.out.println("Taliis Shadow Tool (c)2007-2008 by Mae, Ganku");
		System.out.println("                   Visit http://taliis.blogspot.com !!\n");
		
		if(args.length==0) {
			System.out.println("Usage: java -jar shadowtool <input file> [<output file>]");
			System.exit(0);
		}
		
		else if(args.length==1) {
			inFile = args[0];
			
			if(inFile.contains(".adt"))
				outFile = inFile.replace(".adt", ".png");
			else if(inFile.contains(".png"))
				outFile = inFile.replace(".png", ".adt");
			else {
				System.out.println("Usage: java -jar shadowtool <input file> [<output file>]");
				System.exit(-1);
			}
		}
		else if(args.length==2) {
			inFile = args[0];
			outFile = args[1];
		}
		else System.exit(-2);
		
		new shadowTool(inFile, outFile);
	}

}
