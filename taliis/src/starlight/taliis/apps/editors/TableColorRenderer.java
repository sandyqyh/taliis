package starlight.taliis.apps.editors;

import javax.swing.table.*;
import java.awt.*;

public class TableColorRenderer extends DefaultTableCellRenderer { 
	Color farbe;
	
	public TableColorRenderer(Color col) {
		farbe = col;
	}
	
	@Override 
	public void setValue( Object value ) { 
		setBackground(farbe);
		if ( value instanceof Long ) { 
			setText( value.toString() );
		} 
		else if( value instanceof AdvJTableCell ) {
			AdvJTableCell tmp = (AdvJTableCell)value;
			super.setValue( tmp.value );
			setToolTipText(tmp.Tooltip);
		}
		else 
			super.setValue( value ); 
	}
}