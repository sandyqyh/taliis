package starlight.taliis.apps.editors;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;


import starlight.alien.*;
import starlight.taliis.core.files.dbc;


import java.awt.Dimension;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.Vector;


public class dbcSimpleTable extends JPanel {
    private dbc cUplink;

    JTable table;
    
    public dbcSimpleTable(dbc daddy) {
    	super(new GridLayout(1,0));
    	
    	cUplink = daddy;

        table = new JTable(new dbcTableModel(cUplink));
        //table.setPreferredScrollableViewportSize(new Dimension(70, 70));

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.addComponentListener(new
                CorrectStrangeBehaviourListener(table, scrollPane)); 
        
        //Add the scroll pane to this panel.
        add(scrollPane);
        
        update();
    }
    
    
    /**
     * Data got updated!
     */
    public void update() {    	
    	for(int c=0; c<cUplink.getNFields(); c++) {
    		TableColumn column = table.getColumnModel().getColumn(c);
    		
    		/*if(cUplink.vColTypes.size()==0) column.setMaxWidth(50);
    		// string?
    		else if((Short)cUplink.vColTypes.get(c)==cUplink.string) {
    			column.setMinWidth(30);
    			column.setPreferredWidth(50);
    			column.setCellRenderer(new TableColorRenderer(new Color(255,224,224)));
    		}
    			
    		else {/**/
    			column.setMinWidth(30);
    			column.setPreferredWidth(60);
    		//}    		
    	}
    }
}



class dbcTableModel extends AbstractTableModel {
	dbc archive;
	
	// Bezeichnungen
    Vector columnNames = new Vector();
    
    dbcTableModel(dbc reference) {
    	archive = reference;
    	
    	//TODO: bezeichnungen laden
    	for(int c=0; c<archive.getNFields(); c++) {
    		columnNames.add(c, "#" + c); 
    	}
    }
    
    public int getColumnCount() {
        return (int)archive.getNFields();
    }

    public int getRowCount() {
    	return (int)archive.getNRecords();
    }

    public String getColumnName(int col) {
        return (String) columnNames.get(col);
    }

    public Object getValueAt(int row, int col) {
    	return archive.getData(col, row);// Fields[row][col];
    }

/*    public Class getColumnClass(int c) {
        return dbc;
    }*/

    public boolean isCellEditable(int row, int col) {
    	return false;
    }

    public void setValueAt(Object value, int row, int col) {
    }
    
    public void clear() {
    }
}