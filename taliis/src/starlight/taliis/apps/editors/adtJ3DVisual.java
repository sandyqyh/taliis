package starlight.taliis.apps.editors;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import starlight.taliis.apps.editors.adt3D.Display3D;
import starlight.taliis.core.files.adt;



public class adtJ3DVisual extends JPanel {
	adt obj;
	
    /**
     * Setups J3D view of the given ADT file (mech only)
     * @param rADTFile
     */
	public adtJ3DVisual(adt rADTFile) {
		obj = rADTFile;
		setLayout(new BorderLayout());
		
		if(hasJ3D()) {
			add(new Display3D( obj ), BorderLayout.CENTER);
		}
		else {
			add(
				new JLabel("Unable to show preview because J3D is not installed"),
				BorderLayout.CENTER
			);
			return;
		}
	}
	
	/**
	 * Do we have an installed J3D ?
	 * @return
	 */
	private boolean hasJ3D() {
		try {
			Class.forName("com.sun.j3d.utils.universe.SimpleUniverse");
			Class.forName("javax.media.j3d.Canvas3D");
			return true;
		} catch(	Exception e) {
			System.err.println("No J3D installled");
			return false;
		}
	}
}
