package starlight.taliis.apps.editors;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

import starlight.taliis.core.ZeroTerminatedString;

public class TableZTSEditor extends AbstractCellEditor implements 
TableCellEditor {
    private JTextField component = new JTextField();
    
    public Component getTableCellEditorComponent(
            JTable table, Object value,
            boolean isSelected, int rowIndex, int colIndex )
    {
    	if(value instanceof ZeroTerminatedString) {
    		ZeroTerminatedString tmp = (ZeroTerminatedString)value;
    		component.setText("" + tmp.getInitOffset());
    	}
    	return component;
    }

    public Object getCellEditorValue()
    {
        return component.getText();
    }
}