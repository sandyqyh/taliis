package starlight.taliis.apps.editors.adt3D;

import java.awt.Color;

import javax.media.j3d.Appearance;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Material;
import javax.media.j3d.PolygonAttributes;
import javax.media.j3d.Shape3D;
import javax.media.j3d.TextureUnitState;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.TriangleStripArray;
import javax.vecmath.Color3f;
import javax.vecmath.Point3f;
import javax.vecmath.TexCoord2f;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.geometry.Sphere;

import starlight.taliis.core.chunks.*;
import starlight.taliis.core.chunks.adt.MCNK;
import starlight.taliis.core.files.*;

/**
 * This is the 3th complete redo of the class
 * that is done to display a MCNK chunk in 3D.
 * 
 * I hate it ..
 * 
 * @author tharo
 *
 */
public class FieldBranch {
	// main j3d stuff
	BranchGroup root;	
	Appearance app;
	TriangleStripArray strip;
	TextureUnitState textureUnitState[] = new TextureUnitState[10];
	PolygonAttributes polyAttr;
	TransformGroup transform;
	
	// coord infos
	double xOffset = 0, yOffset = 0, zOffset = 0;

	// display infos
	boolean layerVisibilty[] = new boolean[5];
	
	// Given Datas
	MCNK data;
	//myTextureLoader texLoader;
	
	// my data
	Point3f lines[][];				// all strip points
	int stripLenghts[];				// lenght of all lines
	int vertexCount = 0;
	//TODO: all "speres" located at the points
	
	double scale = 1;
	
	/**
	 * Creates the BranchGroup (root) including the whole
	 * display settings by default.
	 * 
	 * @param xOffs		Where do we have to be displayed (absolute)
	 * @param yOffs		Same
	 */
	public FieldBranch(MCNK reference, int xOffs, int yOffs, double scale) {
	// store
		data = reference;
		double globalSize = MCNK.gfxWidth * scale;
		this.scale = scale;
		
	//TODO: use translation group install of absolute coordinate offsets ..
	// coordinates
		xOffset = xOffs*globalSize;
		yOffset = yOffs*globalSize;
		zOffset = data.getPosZ()*scale;
		
	// set all layers visible
		for(int c=0; c<5; c++)
			layerVisibilty[c] = true;
		
	// Init root Branch
		root = new BranchGroup();
	
	// 3D Strip storage
		lines = new Point3f[8][];
		stripLenghts = new int[8];
		
		setupStrip();
		
	// setup appeareance
		// polygon mode 
		polyAttr = new PolygonAttributes();
        polyAttr.setCullFace( PolygonAttributes.CULL_NONE );
        polyAttr.setPolygonMode(PolygonAttributes.POLYGON_LINE); //POLYGON_FILL);

        // appearance
		app = new Appearance();
		
		Material mat = new Material();
		mat.setLightingEnable(true);
		mat.setDiffuseColor(1.0f, 1.0f, 1.0f);
		
		app.setPolygonAttributes(polyAttr);
		app.setTextureUnitState(textureUnitState);
		app.setMaterial(mat);
		
		// add to root node
		root.addChild(new Shape3D(strip, app));	
	}
	
	
	
	
//	--------------------------------------------------------------------------------
// srtips
	
	/** 
	 * Creates coordinates for all Strips and setup the
	 * Triangle Array 
	 */
	private void setupStrip() {
		// create stripes 
		for(int y=0;y < 8;y++){
			lines[y] = CreateStripLineOfY(y);
			
			vertexCount += lines[y].length;
			stripLenghts[y] = lines[y].length;
		}
		
		// triangle array setup
		strip = new TriangleStripArray(vertexCount, 
				TriangleStripArray.COORDINATES | 
				TriangleStripArray.NORMALS | 
				TriangleStripArray.TEXTURE_COORDINATE_2, stripLenghts);

		strip.setName("Field" + data.getIndexX() + "/" + data.getIndexY());
		
		
		// position of the data in in the strip data space
		int strippos = 0;
		
		// copy the vertex data into the strip
		for(int y=0;y < 8;y++){
			strip.setCoordinates(strippos, lines[y]);
			strippos += lines[y].length;
			
			// generate normals 		TODO: READ normals?
			//Vector3f normals[] = CreateNormalsStripLineOfY(y);
			Vector3f normals[] = getNormalsStripLineOfY(y);
			strip.setNormals(y * normals.length, normals);
			
			// generate texture coordinates
			//TexCoord2f texcoords[] = CreateTextureCoordinatesStripLineOfY(hf,y,1f);
			//strip.setTextureCoordinates(0, y * texcoords.length, texcoords);
		
			// generate drag and drop spheres
			//generateSpheres(lines[y]);
		}
	}

	
	/**
	 * creates the points for one strip line (the y-th) of hf 
	 * @param y witch of our strip lines do we use?
	 * @return
	 */
	private Point3f[] CreateStripLineOfY(int y) {
		// number of vertexes needed
		int count = 1 + 1 + 8*2;
		
		// distance 
		double distance = MCNK.gfxWidth / 8;
		
		Point3f line[] = new Point3f[count];
		
		for(int i = 0;i < count; ++i){
			// position in hf data
			int hfx = (i - (i % 2)) / 2;
			int hfy = y + i % 2;
			
			// position in 3d space
			float px = (float)(xOffset + (hfx)*distance*scale);
			float py = (float)(yOffset + (hfy)*distance*scale);
			float pz = (float)(zOffset + data.mcvt.getValNoLOD(hfx, hfy)*scale);
			
			line[i] = new Point3f(px, py, pz);
		}
		return line;
	}


	private Vector3f[] getNormalsStripLineOfY(int y) {
		int count = 1 + 1 + 8*2;
		Vector3f normals[] = new Vector3f[count];
		
		for(int i = 0;i < count; ++i){
			int hfx = (i - (i % 2)) / 2;
			int hfy = y + i % 2;
			
			byte tmp[] = data.mcnr.getValNoLOD(hfx, hfy);
			normals[i] = new Vector3f( 
					(float)1/127*tmp[0],
					(float)1/127*tmp[1],
					(float)1/127*tmp[2]
				);
		}
		
		return normals;
	}
	/**
	 * Create normal Strip for our line of Data
	 * @param hf
	 * @param y
	 * @return
	 */
	/*private Vector3f[] CreateNormalsStripLineOfY(int y) {
		// number of vertexes needed
		int count = 1 + 1 + 8*2;
		
		Vector3f normals[] = new Vector3f[count];
		
		for(int i = 0;i < count; ++i){
			// position in hf data
			int hfx = (i - (i % 2)) / 2;
			int hfy = y + i % 2;
			
			// calculate normal average of the 4 neighbor points along the x and y axis
			Vector3f l[] = new Vector3f[4];
			
			// generate normals
			l[0] = GenerateNormal(hfx, hfy, hfx + 1, hfy + 0, hfx + 0, hfy + 1);
			l[1] = GenerateNormal(hfx, hfy, hfx + 0, hfy + 1, hfx - 1, hfy + 0);
			l[2] = GenerateNormal(hfx, hfy, hfx - 1, hfy + 0, hfx + 0, hfy - 1);
			l[3] = GenerateNormal(hfx, hfy, hfx + 0, hfy - 1, hfx + 1, hfy + 0);
			
			// calculate average
			float nx = 0.0f;
			float ny = 0.0f;
			float nz = 0.0f;
			
			for(int j=0;j < l.length;++j){
				nx += l[j].x;
				ny += l[j].y;
				nz += l[j].z;
			}
			
			float lcount = l.length;
			normals[i] = new Vector3f(nx / lcount, ny / lcount, nz / lcount);
			normals[i].normalize();
			normals[i].negate();
		}
		
		return normals;
	}/**/
	
	
	
	/**
	 * generates a normal calculated from the base-a and base-b vector
	 * @param hf height field data
	 * @param basex	position in data array
	 * @param basey position in data array
	 * @param ax position in data array
	 * @param ay position in data array
	 * @param bx position in data array
	 * @param by position in data array
	 * @return normal
	 */
	/*private Vector3f GenerateNormal(int basex, int basey, int ax,
			int ay, int bx, int by) {
		
		Vector3f base = new Vector3f(basex,basey,GetHFHeight(hf,basex,basey));
		Vector3f a = new Vector3f(ax,ay,GetHFHeight(hf,ax,ay));
		Vector3f b = new Vector3f(bx,by,GetHFHeight(hf,bx,by));
		
		Vector3f s1 = new Vector3f(a);
		Vector3f s2 = new Vector3f(b);
		
		s1.sub(base);
		s2.sub(base);
		
		Vector3f n = new Vector3f();
		n.cross(s1, s2);
		
		n.normalize();

		return n;
	}/**/
	
	
	public void generateSpheres(Point3f nodes[]){
		for(int i=0; i<nodes.length; i++) {
			Sphere sphere = new Sphere(0.01f);
		
			sphere.setName("Sphere #" + i);
			
			TransformGroup method1Transform = new TransformGroup();
		    Transform3D MoveTheMethod1 = new Transform3D();
			Vector3f method1Vector = new Vector3f(nodes[i]);
		    
			MoveTheMethod1.setTranslation(method1Vector);
		    method1Transform.setTransform(MoveTheMethod1);
		    method1Transform.addChild(sphere);
		    root.addChild(method1Transform);
		}
	}
	
	
//	--------------------------------------------------------------------------------	

	public BranchGroup getBranchGroup() {
		return root;
	}
}
