package starlight.taliis.apps.adtedit;

import javax.swing.*;

import starlight.taliis.helpers.fileLoader;

import java.awt.*;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class infoDialog {
	JFrame frame;
	
	public infoDialog(JFrame parent) {
		// 1. Create the frame.
		frame = new JFrame("About Taliis");
		frame.setLayout(new BorderLayout());
		frame.setLocationRelativeTo(parent);
		frame.setSize(273, 400);
		frame.setResizable(false);
		
		JEditorPane editorPane = new JEditorPane();
		editorPane.setEditable(false);
		URL helpURL = fileLoader.getRessource("files/About.html");
		
		if (helpURL != null) {
		    try {
		        editorPane.setPage(helpURL);
		    } catch (Exception e) {
		        System.err.println("Attempted to read a bad URL: " + helpURL);
		    }
		} else {
		    System.err.println("No ressource found");
		}
		
		
		frame.add(editorPane, BorderLayout.CENTER);

		frame.setVisible(true);
		frame.show();
	}
	/*public static void main(String[] args) {
		new infoDialog(null);
	}/**/
}
