package de.taliis.plugins.wowmpq;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JMenuBar;

import de.taliis.dialogs.mpqSources;
import de.taliis.editor.configMananger;
import de.taliis.editor.fileMananger;
import de.taliis.editor.openedFile;
import de.taliis.editor.plugin.Plugin;
import de.taliis.editor.plugin.PluginRequest;
import de.taliis.editor.plugin.eventServer;

public class mpqFileRequest implements Plugin, PluginRequest {
	fileMananger fm;
	configMananger cm;
	eventServer es;
	
	Vector<String> archives;
	String tmp_dir;
	
	public boolean checkDependencies() { return true; }
	public ImageIcon getIcon() { return null; }
	public int getPluginType() { return PLUGIN_TYPE_REQUEST; }
	public String[] getSupportedDataTypes() { return new String[] { "mpq" }; }
	public String[] neededDependencies() { return null; }
	public void setMenuRef(JMenuBar arg0) {	}
	public void setPluginPool(Vector<Plugin> arg0) { }
	public void setClassLoaderRef(URLClassLoader arg0) { }
	public void unload() { }
	
	public void setEventServer(eventServer arg0) {
		es = arg0;
	}

	public void setFileManangerRef(fileMananger arg0) {
		fm = arg0;
	}
	
	public void setConfigManangerRef(configMananger arg0) {
		cm = arg0;

	// do we have a tmp dir?
		tmp_dir = cm.getGlobalConfig().getProperty("taliis_temp_dir");
		if(tmp_dir==null) tmp_dir = "./temp";
		
	//load MPQ/Priority file List
		reloadArchiveList();
	}

	
	
// Functional Stuff
////////////////////////////////////////////////////////////
	
	
	/**
	 * reload the list of used archives from the config mananger
	 */
	public void reloadArchiveList() {
		archives = new Vector<String>();
		String res = null;
		int i = 0;
		
		do {
			res = cm.getGlobalConfig()
						.getProperty("taliis_mpqLoader_srcFile_"+ (i++));
			if(res!=null) {
				// valid?
				File f = new File(res);
				if(f.canRead())
					archives.add(res);
				else {
					//cm.getGlobalConfig()
					//	.remove("taliis_mpqLoader_srcFile_"+ (i-1));
				}
			}
		} while(res!=null);		
	}
	
	/**
	 * Loads the requested resource by extracting it into
	 * the temporary dir and loading it using the main file
	 * mananger. Then the resource get switched 'invisible'
	 *
	 * If the resource is already extracted it will load just
	 * the already extracted one. 
	 * 
	 * @param filename
	 * @return the opened file handle
	 */
	public File requestFile(String filename) {
		// do a shit?
		if(cm.getGlobalConfig().getProperty("taliis_mpqLoader_dontload")!=null) {
			System.out.println("Note: MPQ File request is dissabled by the user.");
			return null;
		}
			
		
		// IS file allready extracted?
		File dest = new File(tmp_dir + "/" + filename);
		if(dest.canRead()) return dest;
		
		// ckeck tmp listfiles
		for(String s : archives) {
			// file valide and readable?
			File src = new File(s);
			if(!src.canRead()) {
				System.err.println("MPQ File not found " + src.getAbsolutePath());
				continue;
			}
			
			String name = src.getName();
			File listfile = new File(tmp_dir + "/listfile_" + name + ".txt");
			
			// listfile there?
			if(listfile.canRead()==false) {
				extractListFile(src, listfile);
			}
			// still not there? -> skip
			if(listfile.canRead()==false) continue;
			
			// parse for filename
			boolean found = false;
			try {
				BufferedReader in = new BufferedReader(new FileReader(listfile));
				String line = null;
				while ((line = in.readLine()) != null) {
					// sometimes the files in wow have a complete
					// different caps as that in the archive.
					// this will get fixed (a bit) now ..
					if(line.toLowerCase().compareTo(filename.toLowerCase())==0) {
						filename = line;
						found = true;
						break;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			// found?
			if(found==false) continue;
			
			System.out.println(filename + " found in " + name);
				 			
			// extract file
			mpq m = new mpq(src);
	
			try {
				m.parseFile();
				m.extractFile(filename, dest);
				m.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			return dest;
		}
		
		System.out.println(filename + " not found.");
		return null;
	}

	/**
	 * Same as request File but also register the file
	 * in our app. This is only possible if the filetype
	 * is explicit supported by a plugin!
	 * 
	 * @param filename
	 * @return
	 */
	public openedFile requestRessource(String filename) {		
		File dest = requestFile(filename);
		if(dest==null) return null;
		
		// register file
		openedFile of;
		
		try {
			of = fm.openFile(dest);
		} catch(Exception e) {
			return null;
		}
		
		of.setFileOpener(of.OPENED_BY_PLUGIN);

		es.updateTable();
		return of;
	}	
	
	
	/**
	 * Creates an index-list of the files contained
	 * in our mpq file list and save it in the tmp dir.
	 * 
	 * If force is false it will skip all files where
	 * the index file have a newer date then the mpq
	 * archive itself.
	 * 
	 * @param force
	 * 
	 */
	public void createFileList(boolean force) {
		for(String s : archives) {
			String name = new File(s).getName();
			File listfile = new File(tmp_dir + "/listfile_" + name + ".txt");
			File mpqFile = new File(s);
			
			if(mpqFile.canRead()==false) continue;
			
			// update listfiles?
			if( listfile.canRead()==false
				|| listfile.lastModified()<mpqFile.lastModified()
				|| force==true
			) {
				extractListFile(mpqFile, listfile);
			}
		}
	}
	
	/**
	 * Extracts the Listfile of given archive to given destination
	 * @param mpqarchive
	 * @param destiny
	 */
	public void extractListFile(File mpqarchive, File destiny) {
		if(mpqarchive.canRead()==false) return;
		mpq m = new mpq(mpqarchive);
		
		System.out.print("Update mpq listfile for " + mpqarchive.getName() + " .. ");
		try {
			m.parseFile();
			m.extractFile("(listfile)", destiny);
			m.close();

			System.out.println(" done.");
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(" failed.");	
	}
}
